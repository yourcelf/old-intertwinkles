"""
This is an example settings/local.py file.
These settings overrides what's in settings/base.py
"""

import logging

# To extend any settings from settings/base.py here's an example:
#from . import base
#INSTALLED_APPS = base.INSTALLED_APPS + ['debug_toolbar']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'intertwinkles',
        'USER': 'intertwinkles',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    },
}

# Recipients of traceback emails and other notifications.
ADMINS = (
    # ('Your Name', 'your_email@domain.com'),
)
MANAGERS = ADMINS

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'America/New_York'

# Debugging displays nice error messages, but leaks memory. Set this to False
# on all server instances and True only for development.
DEBUG = TEMPLATE_DEBUG = True

# Is this a development instance? Set this to True on development/master
# instances and False on stage/prod.
DEV = True

# Make this unique, and don't share it with anybody.  It cannot be blank.
SECRET_KEY = '&amp;7vid!(vh5-&amp;j*5npz($j_e4_)6+$gdja=hxe5u6eg1+!sh5e!'

# Uncomment these to activate and customize Celery:
CELERY_ALWAYS_EAGER = True  # required to activate celeryd in development
# BROKER_HOST = 'localhost'
# BROKER_PORT = 5672
# BROKER_USER = 'django'
# BROKER_PASSWORD = 'django'
# BROKER_VHOST = 'django'
# CELERY_RESULT_BACKEND = 'amqp'

## Log settings

LOG_LEVEL = logging.INFO
HAS_SYSLOG = True
SYSLOG_TAG = "http_app_intertwinkles"  # Make this unique to your project.
LOGGING = {
    'version': 1,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'null': {
            'level':'DEBUG',
            'class':'django.utils.log.NullHandler',
        },
        'console':{
            'level':'DEBUG',
            'class':'logging.StreamHandler',
            'formatter': 'simple'
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
        }
    },
    'loggers': {
        'intertwinkles': {
            'level': "DEBUG"
        },
        'authproxy.management.commands.authproxyd': {
            'level': "DEBUG"
        },
    }
}

# Common Event Format logging parameters
#CEF_PRODUCT = 'intertwinkles'
#CEF_VENDOR = 'Your Company'
#CEF_VERSION = '0'
#CEF_DEVICE_VERSION = '0'

INTERNAL_IPS = ('127.0.0.1')
SITE_URL = "http://dev.intertwinkles.org/"

# Proxied etherpad and dotstorm instances.  Specify the port on which the proxy
# is listening (e.g., the server spawned by `python manage.py authproxyd`), as
# well as the port on which the backend (etherpad, dotstorm) is listening.
AUTH_PROXIES = ({
        "listen": ":8088", # host:port (or :port for localhost)
        "backend": ":9001", 
        "authorize": 'embeds.auth.authenticate_embeds',
    }, {
        "listen": ":8089",
        "backend": ":9002",
        "authorize": 'embeds.auth.authenticate_embeds',
    }
)
# Specify the public URL, including any trailing path, to the etherpad and
# dotstorm instances.  This might be the same service as the auth proxy
# `listen` host/port above, or you might reverse-proxy those.
EMBED_SERVERS = {
    'etherpad': "https://pad.tirl.org:444/p/",
    'dotstorm': "http://dotstorm.intertwinkles.org/d/",
}

# Enable these options for memcached
CACHES = {
    'default': {
        'BACKEND': "django.core.cache.backends.memcached.MemcachedCache",
        'LOCATION': "127.0.0.1:11211"
    }
}
,
# Set this to true if you use a proxy that sets X-Forwarded-Host
#USE_X_FORWARDED_HOST = False

SERVER_EMAIL = "webmaster@example.com"
DEFAULT_FROM_EMAIL = SERVER_EMAIL
SYSTEM_EMAIL_PREFIX = "[intertwinkles]"

# For development:
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
# For production:
#EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

MAILMAN = dict(
    main_url       = "", # Full URL to mailman web root, e.g. http://example.com/cgi-bin/mailman/
    email_domain   = "", # Full domain for mailman lists, e.g. lists.example.com
    admin_password = "", # List creator's password
    owner_email    = "", # Default list owner password. Set to a real email account that should have admin access to every created list.
    owner_password = "", # A password for the previously mentioned user.  Mailman mails this in cleartext to the default list owner.
    language       = "en",
    list_encoding  = "iso-8859-1",
)
EMBED_SERVERS = {
    'etherpad': "https://pad.tirl.org:444/p/",
    'dotstorm': "http://dotstorm.intertwinkles.org/d/",
}
BROWSERID_CREATE_USER = True
SESSION_COOKIE_DOMAIN = "." + SITE_URL
