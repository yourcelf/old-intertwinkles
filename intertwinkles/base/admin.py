from django.contrib import admin

from intertwinkles.base.models import EmailSignup

class EmailSignupAdmin(admin.ModelAdmin):
    list_display = ['email', 'created']
    search_fields = ['email']
    date_hierarchy = 'created'
admin.site.register(EmailSignup, EmailSignupAdmin)
