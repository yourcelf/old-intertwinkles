from django.db import models
from django.utils.timezone import now

class EmailSignup(models.Model):
    created = models.DateTimeField(default=now)
    email = models.EmailField()

    def __unicode__(self):
        return self.email
