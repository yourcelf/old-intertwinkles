  $("input.datejs-ui").each (i, el) ->
    id = $(el).attr("data-for-id")
    interpretation = $(".datejs-interpretation[data-for-id=#{id}]")
    interpret = (date) ->
      if date?
        interpretation.html "Recognized as:<br />#{date.toString('yyyy MMM d, h:mm tt (dddd)')} <i>#{jstz.determine().name().replace("_", " ")}</i>"
        $(el).parents(".control-group").removeClass("error")
      else
        interpretation.html "Date format not recognized. Try:<br /><tt>#{Date.now().toString('yyyy-MM-dd hh:mm tt')}</tt>"
        $(el).parents(".control-group").addClass("error")
    form_output = $("##{id}")
    user_input = $("input[data-for-id=#{id}]")
    # Initialize
    starting_date_str = form_output.val()
    if starting_date_str
      # Convert to ISO from whatever it is that python gives us.
      starting_date_str = starting_date_str.replace("+00:00", "Z").replace(" ", "T")
      starting_date = new Date(starting_date_str)
      interpret(starting_date)
      user_input.val(starting_date.toString('yyyy MMM d, h:mm tt'))
    
    # Bind
    keyTimeout = null
    user_input.on "keyup", ->
      if keyTimeout
        clearTimeout(keyTimeout)
      user_input.addClass("loading")
      keyTimeout = setTimeout ->
        user_input.removeClass("loading")
        val = user_input.val()
        if val
          date = Date.parse(user_input.val())
          interpret(date)
        else
          $(el).parents(".control-group").removeClass("error")
          interpretation.html("")
        if date?
          form_output.val(date.toISOString())
        else
          form_output.val("")
      , 300
