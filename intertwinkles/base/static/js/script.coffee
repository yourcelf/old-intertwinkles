window.it ||= {
  EMAIL_RE: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i
}

$(document).ready ->
  #
  # Handle date input widgets
  #
  $("[rel=tooltip]").tooltip()
  $("[data-rel=tooltip]").tooltip()
  #
  # Modal confirmations
  #
  current_form = null
  $("[data-confirm]").on "click", ->
      submit_button = $(this)
      current_form = submit_button.parents("form")
      modal_selector = submit_button.attr("data-confirm")
      $(modal_selector).modal()
      $("[data-submit]").on "click", ->
        # Simulate pressing the same submit button.
        if current_form?
          current_form.append $("<input/>").attr({
            name: submit_button.attr("name")
            vlaue: submit_button.val()
          })
          current_form.submit()
      return false
 
  $("span.intertwinkles").on "mouseover", ->
    $el = $(this)
    unless $el.hasClass("twunkled")
      $el.addClass("twunkled")
      letters = $el.text()
      spans = []
      for i in [0...letters.length]
        spans.push("<span>#{letters.substr(i, 1)}</span>")
      $el.html(spans.join(""))
    $el.find("span").each (i, el)->
      setTimeout( ->
        el.className = "bump"
        setTimeout((-> el.className = ""), 100)
      , i * 50)

  $(".modal-video").on "click", ->
    width = parseInt($(this).attr("data-width"))
    height = parseInt($(this).attr("data-height"))
    mod = $("<div class='modal' role='dialog'></div>").css {
      display: "none"
      width: "#{width + 10}px"
      height: "#{height + 10}px"
      "background-color": "black"
      "text-align": "center"
      padding: "5px 5px 5px 5px"
    }
    mod.append("<iframe width='#{width}' height='#{height}' src='#{$(this).attr("data-url")}?autoplay=1&cc_load_policy=1' frameborder='0' allowfullscreen></iframe>")
    $("body").append(mod)
    mod.on('hidden', -> mod.remove())
    mod.modal()
    return false

  $("[rel=popover]").popover()

  #$('[data-spy="affix"]').affix()
