from django.http import HttpResponseNotAllowed
from django.template import RequestContext
from django.template import loader

class HttpResponseNotAllowedMiddleware(object):
    """
    Display a custom response page for 405 responses (http method not allowed).
    """
    def process_response(self, request, response):
        if isinstance(response, HttpResponseNotAllowed):
            context = RequestContext(request)
            # Reuse the 'PermissionDenied' template; it's semantically accurate
            # to an end user.
            response.content = loader.render_to_string("403.html", context_instance=context)
        return response
