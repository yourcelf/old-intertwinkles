from django import forms
from django.utils.safestring import mark_safe

class DateJSWidget(forms.TextInput):
    class Media:
        js = ('js/libs/date.js', 'js/libs/detect_timezone.js', 'js/date_widget.js')

    def render(self, name, value, attrs=None):
      return mark_safe(
          u"""
          <input type='text' data-for-id='%(id)s' class='datejs-ui' 
            placeholder='e.g. July 7, 8pm'
            />
          <input type='hidden' name='%(name)s' id='%(id)s' value='%(value)s' />
          <span class='datejs-interpretation help-inline' data-for-id='%(id)s'></span>
          """ % {
            'name': name,
            'id': attrs.get('id', id(self)),
            'value': value or "",
          })

class EmailForm(forms.Form):
    email = forms.EmailField()

