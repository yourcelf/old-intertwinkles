"""urlconf for the base application"""

from django.conf.urls.defaults import url, patterns, include

urlpatterns = patterns('intertwinkles.base.views',
    url(r'^r/', include('shorty.urls')),
    url(r'^$', 'home', name='home'),
)
