import json
from django.shortcuts import render, redirect
from django.template import RequestContext
from django.http import HttpResponse
from django.contrib import messages
from django.core.mail import mail_managers
from django.conf import settings
from django import forms

from intertwinkles.base.models import EmailSignup
from intertwinkles.base.forms import EmailForm

from groups.views import manage_groups

def home(request):
    """ Default view for the root """
    if request.user.is_authenticated():
        return manage_groups(request)

    if request.method == 'POST':
        form = EmailForm(request.POST)
        if form.is_valid():
            emsu, created = EmailSignup.objects.get_or_create(
                email=form.cleaned_data['email']
            )
            if created:
                mail_managers(
                    subject="New InterTwinkles email signup",
                    message="New signup from: %s.\n%s" % (
                        emsu.email,
                        "%s%s" % (
                            settings.SITE_URL,
                            "/admin/base/emailsignup/%s" % emsu.pk
                        )
                    )
                )
            messages.info(request,
                "Thanks!  We hope to get in touch with you soon."
            )
            return redirect("home")
        else:
            messages.error(request,
                "We can't recognize '%s' as an email address.  Try another address?" % (
                    request.POST.get('email')
                )
            )
    return render(request, 'base/home.html')

