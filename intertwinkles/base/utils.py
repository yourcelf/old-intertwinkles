import time
from datetime import datetime

def json_dt_handler(obj):
    if isinstance(obj, datetime):
        # Proper UTC formatting (with Z) from the UTC version of given
        # datetime.
        return time.strftime('%Y-%m-%dT%H:%M:%SZ', obj.utctimetuple())
    return None

