import time
from django.test import LiveServerTestCase
from django.contrib.auth.models import User

from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys

from profiles.models import Profile
from groups.models import Group, Membership
from embeds.models import Embed
from resolve.models import Proposal, Opinion
from timelines.models import Event

def hard_click(el, browser):
    """
    Sometimes selenium seems to be unable to properly click.  This is more
    reliable.
    """
    chain = ActionChains(browser)
    chain.click_and_hold(el)
    chain.release(None)
    chain.perform()


#
# Integration tests
#
class TestInterTwinkles(LiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(4)
        super(TestInterTwinkles, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        super(TestInterTwinkles, cls).tearDownClass()
        cls.selenium.quit()

    def do_login(self, user="one@mockmyid.com"):
        with self.settings(SITE_URL=self.live_server_url):
            s = self.selenium
            s.get("%s%s" % (self.live_server_url, '/profiles/signin/'))
            s.find_element_by_id("browserid").click()
            s.switch_to_window(s.window_handles[1])
            s.find_element_by_id("email").send_keys("one@mockmyid.com")
            for el in s.find_elements_by_tag_name("button"):
                if el.text in ("next", "sign in"):
                    el.click()
            s.switch_to_window(s.window_handles[0])

            WebDriverWait(s, 5).until(
                lambda s: "one@mockmyid.com" in s.find_element_by_class_name("user-menu").text
            )

    def do_new_profile(self):
        s = self.selenium
        s.find_element_by_id("id_name").send_keys("Me")
        s.find_element_by_class_name("icon").click()
        s.find_element_by_xpath("//input[@type='submit']").click()

    def add_new_group(self):
        s = self.selenium
        s.get("%s%s" % (self.live_server_url, '/groups/'))
        group_menu = s.find_element_by_id("group_menu")
        group_menu.find_element_by_class_name("dropdown-toggle").click()
        group_menu.find_elements_by_tag_name("a")[-1].click()
        s.find_element_by_id("id_name").send_keys("My Group")
        # No mailing list for test users...
        ml = s.find_element_by_id("id_enable_mailing_list")
        ml.click()
        self.assertEquals(bool(ml.get_attribute("checked")), False)
        s.find_element_by_xpath("//input[@type='text' and @name='email']").send_keys(
                "two@mockmyid.com")
        s.find_element_by_class_name("btn-primary").click()

        group = Group.objects.get(name="My Group")
        self.assertTrue(
            group.membership_set.filter(user__email="one@mockmyid.com").exists()
        )

    def add_etherpad(self):
        s = self.selenium
        s.get("%s%s" % (self.live_server_url, '/groups/show/my-group'))
        s.find_element_by_link_text("New document").click()
        s.find_element_by_id("id_name").send_keys("My Etherpad")
        s.find_element_by_id("id_type").send_keys("etherpad")
        s.find_element_by_class_name("btn-primary").click()

        self.assertTrue(
            "My Etherpad" in s.find_element_by_tag_name("title").text
        )
        self.assertTrue(
            Embed.objects.filter(name="My Etherpad", type="etherpad").exists()
        )

    def add_dotstorm(self):
        s = self.selenium
        s.get("%s%s" % (self.live_server_url, '/groups/show/my-group'))
        s.find_element_by_link_text("New document").click()
        s.find_element_by_id("id_name").send_keys("My Dotstorm")
        s.find_element_by_id("id_type").send_keys("dotstorm")
        s.find_element_by_class_name("btn-primary").click()

        self.assertTrue(
            "My Dotstorm" in s.find_element_by_tag_name("title").text
        )
        self.assertTrue(
            Embed.objects.filter(name="My Dotstorm", type="dotstorm").exists()
        )

    def add_proposal_and_response_and_twinkle(self):
        s = self.selenium
        s.get("%s%s" % (self.live_server_url, '/groups/show/my-group'))
        s.find_element_by_link_text("New proposal").click()
        s.find_element_by_id("id_text").send_keys("Be it resolved...")
        s.find_element_by_class_name("btn-primary").click()
        
        self.assertTrue("Be it resolved" in s.find_element_by_tag_name("title").text)
        p = Proposal.objects.get(proposalrevision__text="Be it resolved...")
        rev = p.current_revision()

        s.find_element_by_link_text("Respond").click()
        s.find_element_by_id("id_choice").send_keys("Approve with reservations")
        s.find_element_by_id("id_text").send_keys("I think that...")
        s.find_element_by_class_name("btn-primary").click()

        self.assertTrue("Be it resolved" in s.find_element_by_tag_name("title").text)
        op = Opinion.objects.get(
                proposal__proposalrevision__text="Be it resolved...",
                user__email="one@mockmyid.com")
        self.assertTrue(op.is_current_denormalized)


        def get_twink():
            twinks = s.find_elements_by_class_name("twinkles")
            for a in twinks:
                if a.get_attribute("data-proposal-id") == str(p.pk):
                    return a

        a = get_twink()
        #a.click() # Didn't work.
        #hard_click(a, s) # Didn't work.
        a.find_element_by_tag_name("img").click() # worked..

        WebDriverWait(s, 5).until(
            lambda s: "(active)" in get_twink().find_element_by_tag_name(
                "img").get_attribute("alt")
        )
       
        self.assertEquals(Event.objects.involving_model(p).count(), 4)

    def test_intertwinkles(self):
        """
        Given the overhead of building a browser, establishing a login, etc.;
        chain them.
        """
        self.do_login()
        self.do_new_profile()
        self.add_new_group()
        self.add_etherpad()
        self.add_dotstorm()
        self.add_proposal_and_response_and_twinkle()
