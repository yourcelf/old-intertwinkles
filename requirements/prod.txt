# Install everything that needs to be compiled
-r compiled.txt

# Django stuff
Django>=1.4
django-model-utils
django-jsonfield
-e git://github.com/mozilla/django-browserid.git#egg=django_browserid
-e git://github.com/yourcelf/django-notification@sms#egg=django_notification
-e git://github.com/yourcelf/django-authproxy#egg=django-authproxy
-e git://github.com/yourcelf/django-mailman#egg=django-mailman
-e git://github.com/yourcelf/django-fence#egg=django-fence
johnny-cache
#django-haystack>=2.0
-e git://github.com/toastdriven/django-haystack#egg=django-haystack
pysolr
django_compressor
BeautifulSoup
sorl-thumbnail

# Security
bleach

# Logging
#raven

# Celery: Message queue
celery
django-celery

# L10n/i18n
pytz

# Caching
python-memcached

# Admin
django-debug-toolbar
# There are useful panels for debug-toolbar, but have not quite been updated yet for 1.4
#-e git://github.com/playfire/django-debug-toolbar-user-panel#egg=django_debug_toolbar_user_panel-dev
#-e git://github.com/ross/memcache-debug-panel#egg=memcache_toolbar-dev

# Migrations
South

# Analytics
django-analytical
