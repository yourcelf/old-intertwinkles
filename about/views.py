from django.shortcuts import render
# Create your views here.
def about(request):
    return render(request, "about/about.html")

def terms(request):
    return render(request, "about/terms.html")

def privacy(request):
    return render(request, "about/privacy.html")

def related(request):
    return render(request, "about/related.html")
