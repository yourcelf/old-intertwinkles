from django.conf.urls.defaults import include, patterns, url

urlpatterns = patterns('about.views',
    url(r'^$', 'about', name='about'),
    url(r'^terms/$', 'terms', name='terms'),
    url(r'^privacy/$', 'privacy', name='privacy'),
    url(r'^related/$', 'related', name='related'),
)
