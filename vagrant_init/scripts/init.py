#!/usr/bin/env python

from common import *

# script("ssh.py") -- overwrites sshd_config; skip for now.
script("hostname.py")
script("system_datetime.py")

script("firewall_1_install.py")
script("firewall_2_prepare.py")
script("firewall_3_configure.py")
script("firewall_5_finalize.py")

script("system_update.py")
script("base_utilities.py")

script("postgres_1_install.py")
script("postgres_3_configure.py")
script("postgres_5_finalize.py")

if config.ENABLE_MAIL:
    script("mail_1_install.py")
    script("mail_3_configure.py")
    script("mail_5_finalize.py")
    script("mailman_1_install.py")
    script("mailman_3_configure.py")

script("node_1_install.py")

script("etherpad_1_install.py")
script("etherpad_3_configure.py")
script("etherpad_5_finalize.sh")

script("dotstorm_1_install.py")
script("dotstorm_3_configure.py")
script("dotstorm_5_finalize.sh")

script("solr_1_install.py")
script("solr_3_configure.py")

script("intertwinkles_1_install.py")
script("intertwinkles_3_configure.py")
script("intertwinkles_3_management.py")
script("intertwinkles_5_finalize.py")

script("apache_1_install.py")
script("apache_3_config.py")
script("apache_5_finalize.py")

script("haproxy_1_install.py")
script("haproxy_3_configure.py")
script("haproxy_5_finalize.py")

script("nagios_1_install.py")
