#!/usr/bin/env python

from common import *

# Set django settings
write_template("/var/intertwinkles/intertwinkles/settings/local.py", extra_context={
    'DEBUG': "True" if config.DEV_MODE else "False",

})
write_template("/etc/supervisor/conf.d/intertwinkles-auth-proxy.conf")

create_postgres_user(
        config.INTERTWINKLES_DATABASE_USER,
        config.INTERTWINKLES_DATABASE_PASSWORD)
create_postgres_database(
        config.INTERTWINKLES_DATABASE_USER,
        config.INTERTWINKLES_DATABASE_NAME)
