#!/usr/bin/env python

from common import *

write_template("/etc/dovecot/dovecot-sql.conf", perms="0600", extra_context={
    'DOVECOT_USE_SSL': "yes" if config.MAIL_ENABLE_SSL else "no"})
write_template("/etc/intertwinkles/bin/intertwinkles-email", perms="0700")

# Dovecot
create_postgres_user(config.MAIL_DB_USER, config.MAIL_DB_PASSWORD)
create_postgres_database(config.MAIL_DB_USER, config.MAIL_DB_NAME)
write_template("/etc/dovecot/dovecot.conf")

# Postfix
# NOTE: /etc/postfix/main.cf written in mail_1_install.py
write_template("/etc/postfix/master.cf")
write_template("/etc/postfix/admin_aliases")
write_template("/etc/postfix/list_domains")
write_template("/etc/postfix/virtual_aliases")
write_template("/etc/postfix/transport")
write_template("/etc/postfix/prembox_virtual_aliases")
write_template("/etc/postfix/virtual_domains")
write_template("/etc/postfix/virtual_mailboxes")

sh("/usr/sbin/postmap /etc/postfix/admin_aliases")
sh("/usr/sbin/postmap /etc/postfix/list_domains")
sh("/usr/sbin/postmap /etc/postfix/virtual_aliases")
sh("/usr/sbin/postmap /etc/postfix/transport")
sh("/usr/sbin/postmap /etc/postfix/prembox_virtual_aliases")
sh("/usr/sbin/postmap /etc/postfix/virtual_domains")
sh("/usr/sbin/postmap /etc/postfix/virtual_mailboxes")


# SSL
if config.MAIL_CERTIFICATE_AUTHORITY_CHAIN:
    write_template("/etc/ssl/certs/mail-ssl-chain.crt")

if config.MAIL_ENABLE_SSL:
    write_template("/etc/ssl/certs/mail.crt")
    write_template("/etc/ssl/private/mail.key", perms="0600")

    if config.MAIL_CERTIFICATE_AUTHORITY_CHAIN and config.MAIL_SSL_CERTIFICATE:
        sh("""
        cd /etc/ssl/certs
        cat mail.crt mail-ssl-chain.crt > mail-with-chain.crt
        """)
    elif config.MAIL_SSL_CERTIFICATE:
        # mail-with-chain is just the mail cert.
        sh("""
        cd /etc/ssl/certs
        cat mail.crt > mail-with-chain.crt
        """)
