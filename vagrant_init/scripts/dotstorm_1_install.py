#!/usr/bin/env python

from common import *

sh("sudo apt-get install -y mongodb redis-server libcairo2 libcairo2-dev")

if not os.path.exists("/var/dotstorm.git"):
    sh("git clone --bare https://github.com/yourcelf/dotstorm.git /var/dotstorm.git")
    sh("chown -R www-data.www-data /var/dotstorm.git")
else:
    sh("cd /var/dotstorm.git ; su www-data -c 'git fetch origin master:master'")

if not os.path.exists("/var/dotstorm/"):
    sh("git clone /var/dotstorm.git /var/dotstorm")
else:
    sh("cd /var/dotstorm ; su www-data -c 'git pull origin master'")
sh("chown -R www-data /var/dotstorm")
sh("cd /var/dotstorm ; su www-data -c 'npm install'")
