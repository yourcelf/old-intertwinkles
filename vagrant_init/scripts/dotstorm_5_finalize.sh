#!/bin/bash

set -e
set -x

/etc/init.d/dotstorm start
update-rc.d dotstorm defaults
