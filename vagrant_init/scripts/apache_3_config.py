#!/usr/bin/env python

import sys
from common import *

extra_context = {'CHAIN_FILE_CONFIG': ""}
#
## We conditionally set the SSLCertificateChainFile declaration depending on
## whether we have a chain file pem to use.
#if config.ENABLE_SSL:
#    if not (config.SSL_CERTIFICATE and config.SSL_KEY):
#        log("SSL enabled, but missing SSL_CERTIFICATE or SSL_KEY config.")
#        sys.exit(1)
#    
#    sh("a2enmod ssl")
#
#    write_template("/etc/apache2/ssl/intertwinkles/InterTwinkles.key", perms="0400")
#    write_template("/etc/apache2/ssl/intertwinkles/InterTwinkles.crt", perms="0400")
#
#    if config.CERTIFICATE_AUTHORITY_PEM:
#        write_template("/etc/apache2/ssl/intertwinkles/ca.pem", perms="0400")
#
#    if config.CERTIFICATE_AUTHORITY_CHAIN_PEM:
#        chain_file = "/etc/apache2/ssl/intertwinkles/TrustChain.pem"
#        write_template(chain_file, perms="0400")
#        extra_context['CHAIN_FILE_CONFIG'] = \
#            "SSLCertificateChainFile %s" % chain_file


write_template("/etc/apache2/ports.conf")
write_template("/etc/apache2/site-includes/intertwinkles")
write_template("/etc/apache2/sites-available/intertwinkles",
               extra_context=extra_context)

sh("a2enmod wsgi rewrite")
sh("a2ensite intertwinkles")
