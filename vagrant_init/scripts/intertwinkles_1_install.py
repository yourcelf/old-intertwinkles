#!/usr/bin/env python

from common import *
import urllib2

# System dependencies...
sh("apt-get install -y libevent-dev inkscape imagemagick memcached")

sh("mkdir -p /var/git")
if not os.path.exists("/var/git/intertwinkles.git"):
    sh("git clone --bare https://yourcelf@bitbucket.org/yourcelf/intertwinkles.git /var/git/intertwinkles.git")
else:
    sh("cd /var/git/intertwinkles.git ; git fetch origin master:master")

if not os.path.exists("/var/intertwinkles"):
    sh("git clone /var/git/intertwinkles.git /var/intertwinkles")
else:
    sh("cd /var/intertwinkles/ && git pull origin master")

if not os.path.exists("/var/intertwinkles/venv"):
    sh("virtualenv /var/intertwinkles/venv")

sh("/var/intertwinkles/venv/bin/pip --quiet install -r /var/intertwinkles/requirements/prod.txt")
