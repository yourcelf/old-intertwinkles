#!/usr/bin/env python

from common import *

if not os.path.exists("/var/etherpad-lite/"):
    sh("git clone https://github.com/yourcelf/etherpad-lite.git /var/etherpad-lite/")
else:
    sh("cd /var/etherpad-lite ; git pull origin master")
# Install as www-data, so that etherpad can check its dependencies on startup
# as www-data.
sh("chown -R www-data.www-data /var/etherpad-lite/ /var/www/")
sh("cd /var/etherpad-lite ; su www-data -c 'bin/installDeps.sh && npm install pg'")
sh("chown -R www-data.www-data /var/etherpad-lite/ /var/www/")
