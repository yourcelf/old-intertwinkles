#!/usr/bin/env python

from common import *

sh("apt-get install -y git")
sh("apt-get install -y mercurial")
sh("apt-get install -y python-pip python-virtualenv python-dev")
sh("apt-get install -y build-essential")
sh("apt-get install -y supervisor")
sh("apt-get install -y curl")
sh("apt-get install -y vim")
sh("apt-get install -y iamerican-small")
