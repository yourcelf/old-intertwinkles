#!/usr/bin/env python

from common import *

VERSION = "3.6.1"

sh("apt-get install -y openjdk-7-jre")
if not os.path.exists("/usr/local/share/apache-solr-{0}".format(VERSION)):
    sh("""
        cd /usr/local/share/
        wget http://mirror.metrocast.net/apache/lucene/solr/{0}/apache-solr-{0}.tgz
        tar xzf apache-solr-{0}.tgz
        rm apache-solr-{0}.tgz
        rm -f /usr/local/share/solr
        ln -s /usr/local/share/apache-solr-{0} /usr/local/share/solr
        mkdir -p /usr/local/share/apache-solr-{0}/example/solr/data
        chown -R www-data.www-data /usr/local/share/apache-solr-{0}/example/solr/data/
    """.format(VERSION))
