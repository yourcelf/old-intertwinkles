#!/usr/bin/env python

from common import *

write_template("/var/dotstorm/config.json")
write_template("/var/dotstorm/bin/safeRun.sh", perms="0755")
write_template("/var/dotstorm/bin/run.sh", perms="0755")
write_template("/etc/init.d/dotstorm", perms="0755")
sh("touch /var/log/dotstorm.log ; chown www-data.www-data /var/log/dotstorm.log")
sh("mkdir -p /var/dotstorm/assets/uploads/")
sh("chown -R www-data.www-data /var/dotstorm/assets/uploads/")
