#!/usr/bin/env python

from common import *

import subprocess

if os.path.exists("/var/run/etherpad-lite.pid") and os.path.exists("/etc/init.d/etherpad-lite"):
    sh("/etc/init.d/etherpad-lite stop")
