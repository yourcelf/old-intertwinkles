#!/usr/bin/env python

from common import *

sh("""
{python_manage} syncdb --noinput
{python_manage} migrate
{python_manage} collectstatic --noinput
{python_manage} build_icon_chooser
{python_manage} thumbnail clear
{python_manage} shell <<-EOF
from django.contrib.auth.models import User
u, created = User.objects.get_or_create(
    username="$INTERTWINKLES_ADMIN_USERNAME",
    email="$INTERTWINKLES_ADMIN_EMAIL",
    is_superuser=True)
u.set_password("$INTERTWINKLES_ADMIN_PASSWORD")
u.save()
from django.contrib.sites.models import Site
s = Site.objects.get_current()
s.name = "InterTwinkles"
s.domain = "http://$INTERTWINKLES_DOMAIN_NAME"
s.save()
EOF

chown -R www-data.www-data /var/intertwinkles/static
chown -R www-data.www-data /var/intertwinkles/media

{python_manage} build_solr_schema > /usr/local/share/solr/example/solr/conf/schema.xml
supervisorctl restart intertwinkles-solr
{python_manage} rebuild_index --noinput

""".format(python_manage="/var/intertwinkles/venv/bin/python /var/intertwinkles/manage.py"))
