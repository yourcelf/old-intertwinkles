#!/usr/bin/env python

from common import *

write_template("/etc/mailman/mm_cfg.py")
write_template("/etc/apache2/sites-available/mailman")
sh("rmlist mailman", require_success=False)
sh("""
mmsitepass $MAILMAN_SITE_PASSWORD
mmsitepass -c $MAILMAN_SITE_PASSWORD
newlist -q mailman $MAILMAN_LIST_OWNER_EMAIL $MAILMAN_ADMIN_PASSWORD
""")
#sh("a2ensite mailman")
