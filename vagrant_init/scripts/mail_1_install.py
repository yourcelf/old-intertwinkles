#!/usr/bin/env python

from common import *

sh("apt-get install -y dovecot-imapd dovecot-pop3d dovecot-pgsql maildrop libsasl2-2 libsasl2-modules")

# Postfix is a beast to install non-interactively; we must write /etc/mailname
# and /etc/postfix/main.cf BEFORE apt-getting.
sh('echo "$INTERTWINKLES_DOMAIN_NAME" > /etc/mailname')
write_template("/etc/postfix/main.cf", extra_context={
    'POSTFIX_USE_TLS': "yes" if config.MAIL_ENABLE_SSL else "no"
})
sh("DEBIAN_FRONTEND='noninteractive' apt-get install -y -q --force-yes postfix postfix-pgsql")
