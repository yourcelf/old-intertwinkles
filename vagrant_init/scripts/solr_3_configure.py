#!/usr/bin/env python

from common import *

write_template("/etc/supervisor/conf.d/intertwinkles-solr.conf")
sh("touch /usr/local/share/solr/example/solr/conf/stopwords_en.txt")
sh("touch /var/log/afg-solr.log")
sh("chown www-data.www-data /var/log/afg-solr.log")
