#!/bin/bash

set -e
set -x

/etc/init.d/etherpad-lite start
update-rc.d etherpad-lite defaults
