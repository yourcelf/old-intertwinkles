#!/usr/bin/env python

from common import *

import subprocess

if os.path.exists("/var/run/dotstorm.pid") and os.path.exists("/etc/init.d/dotstorm"):
    sh("/etc/init.d/dotstorm stop")
