#!/usr/bin/env python

from common import *

write_template("/var/etherpad-lite/settings.json")
write_template("/var/etherpad-lite/bin/safeRun.sh", perms="0755")
write_template("/etc/init.d/etherpad-lite", perms="0755")
sh("touch /var/log/etherpad-lite.log ; chown www-data.www-data /var/log/etherpad-lite.log")

create_postgres_user(
        config.INTERTWINKLES_ETHERPAD_DATABASE_USER,
        config.INTERTWINKLES_ETHERPAD_DATABASE_PASSWORD)
create_postgres_database(
        config.INTERTWINKLES_ETHERPAD_DATABASE_USER,
        config.INTERTWINKLES_ETHERPAD_DATABASE_NAME)
