# If 'DEV_MODE' is True, the Intertwinkles Django will be set up with
# DEBUG=True, and mod_wsgi will be set up to restart apache on any code change.
# Set to 'False' for production sites.
DEV_MODE = True
# Host name for server (value for /etc/hostname)
HOSTNAME = "twinkles"
# IP Address given to the VPS.
SERVER_IP_ADDRESS = "18.85.11.172"

#
# Intertwinkles django
#

# Fully qualified DNS name for intertwinkles front.
INTERTWINKLES_DOMAIN_NAME = "dev.intertwinkles.org"
INTERTWINKLES_SERVER_EMAIL = "server@dev.intertwinkles.org"
# Postgres database name
INTERTWINKLES_DATABASE_NAME = "intertwinkles"

# Etherpad-lite
INTERTWINKLES_ETHERPAD_DOMAIN = "etherpad.%s" % INTERTWINKLES_DOMAIN_NAME
INTERTWINKLES_ETHERPAD_PROXY_PORT = 8001
INTERTWINKLES_ETHERPAD_LOCAL_PORT = 9001
INTERTWINKLES_ETHERPAD_DATABASE_NAME = "etherpad"
INTERTWINKLES_ETHERPAD_DATABASE_USER = "etherpad"

# Dotstorm
INTERTWINKLES_DOTSTORM_DOMAIN = "dotstorm.%s" % INTERTWINKLES_DOMAIN_NAME
INTERTWINKLES_DOTSTORM_PROXY_PORT = 8002
INTERTWINKLES_DOTSTORM_LOCAL_PORT = 9002

# Mailman
MAILMAN_EMAIL_HOST = "lists.%s" % INTERTWINKLES_DOMAIN_NAME
MAILMAN_URL_HOST = "lists.%s" % INTERTWINKLES_DOMAIN_NAME
MAILMAN_LIST_OWNER_EMAIL = "list-owner@%s" % INTERTWINKLES_DOMAIN_NAME

# Mail
MAIL_DB_NAME = "intertwinkles_mail"

# Log the installation script's output.
LOGGING = False
