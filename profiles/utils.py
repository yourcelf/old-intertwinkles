from django.conf import settings
from django.contrib.auth.models import User
from django_browserid.auth import default_username_algo
from django.template import loader, Context
from django.core.exceptions import ObjectDoesNotExist

username_algo = getattr(settings, 'BROWSERID_USERNAME_ALGO', default_username_algo)

def get_or_create_user(email):
    try:
        user = User.objects.get(email=email)
    except User.DoesNotExist:
        user = User(username=username_algo(email), email=email, password="tmp")
        user.full_clean()
        user.password = ""
        user.save()
    return user

def user_public_dict(user):
    data = {'type': 'Person', 'id': user.pk, 'email': user.email}
    try:
        profile = user.get_profile()
    except ObjectDoesNotExist:
        profile = None
    if profile:
        data['name'] = profile.name
        data['icon_color'] = profile.icon_color
        data['icon_url'] = profile.get_icon_url('16')
        data['icon_name'] = profile.get_icon_name()
    t = loader.get_template("profiles/partials/person.html")
    data['badge'] = t.render(Context({'user': user}))
    t = loader.get_template("profiles/partials/inline.html")
    data['inline'] = t.render(Context({'user': user}))
    t = loader.get_template("profiles/partials/icon.html")
    data['icon'] = t.render(Context({'user': user}))
    return data

