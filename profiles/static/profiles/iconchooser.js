$(".iconchooser").each(function() {
    var $el = $(this);
    var div = $("<div/>").attr({
        "class": "iconchooser-widget loading"
    });
    $el.after(div);
    var chooser_img = $el.attr("data-icon-images");
    var val = parseInt($el.val());
    for (var i = 0; i < CUTE_AUTH_ICON_DEFS.length; i++) {
        (function(icon) {
            var choose = function(icon_div) {
                $el.val(icon.pk);
                $(".icon .chosen", div).remove();
                $(icon_div).append($("<div class='chosen'></div>"));
            }
            icon_div = $("<div/>").attr({
                    "class": "icon"
                }).css({
                    "background-image": 'url("' + chooser_img + '")',
                    "background-position": (-32 * i) + "px 0px"
                }).on("click", function() {
                    choose(this)
                }).html(icon.name)
            div.append(icon_div);
            if (icon.pk == val) {
                choose(icon_div);
            };
        })(CUTE_AUTH_ICON_DEFS[i]);
    }
    div.append("<div style='float: left;'><a class='attribution-link' href='" + $el.attr("data-icon-attribution-url") + "'>" + 
               $el.attr("data-icon-attribution-text") + "</a></div>")
    div.append("<div style='clear: both;'></div>");
    div.removeClass("loading");
});
