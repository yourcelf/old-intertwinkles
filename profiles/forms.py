from django import forms
from django.conf import settings
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.core.exceptions import ValidationError
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _

from profiles.models import Icon, Profile, EmailChangeRequest

class IconChooser(forms.Widget):
    class Media:
        css = {
            'all': ['profiles/iconchooser.css'],
        }
        js = ['profiles/iconchooser.js']

    def render(self, name, value, attrs=None):
        with open(settings.MEDIA_ROOT + "/profile_icons/chooser.json") as fh:
            icon_defs = fh.read()
        return mark_safe(
            u"""
            <script type='text/javascript'>
                window.CUTE_AUTH_ICON_DEFS = {icon_defs};
            </script>
            <input type='hidden' name='{name}' value='{value}' class='iconchooser'
                        data-icon-images='{icon_images_url}'
                        data-icon-attribution-url='{attribution_url}'
                        data-icon-attribution-text='{attribution_text}'
                    />""".format(
                name=name, value=value,
                icon_defs=icon_defs,
                icon_images_url=settings.MEDIA_URL + "profile_icons/chooser.png",
                attribution_url=reverse("profiles_icon_attribution"),
                attribution_text=_("About these icons"),
            )
        )

class ColorChooser(forms.TextInput):
    class Media:
        js = ('profiles/jscolor/jscolor.js',)

    def __init__(self, *args, **kwargs):
        kwargs['attrs'] = kwargs.get('attrs', {})
        kwargs['attrs']['class'] = kwargs['attrs'].get('class', 'color')
        kwargs['attrs']['size'] = kwargs['attrs'].get('size', 6)
        super(ColorChooser, self).__init__(*args, **kwargs)

class ProfileForm(forms.ModelForm):
    source_icon = forms.ModelChoiceField(queryset=Icon.objects.all(), widget=IconChooser,
            label=_("Choose an icon"))
    icon_color = forms.CharField(widget=ColorChooser, max_length=6, 
            label=_("Choose a color"))
    name = forms.CharField(label="Your name")

    def clean_mobile_carrier(self):
        if self.cleaned_data['mobile_carrier']:
            # Blank the mobile carrier if no number is given.
            if not self.cleaned_data['mobile_number']:
                return ''
        elif self.cleaned_data['mobile_number']:
            # Complain if number is given, but no carrier.
            raise ValidationError("Please specify the carrier.")
        return self.cleaned_data['mobile_carrier']

    class Meta:
        model = Profile
        fields = ('name', 'mobile_number', 'mobile_carrier', 'source_icon', 'icon_color')

class NewProfileForm(forms.ModelForm):
    name = forms.CharField(label="What's your name?")
    icon_color = forms.CharField(widget=ColorChooser, max_length=6,
            label=_("What's your favorite color?"))
    source_icon = forms.ModelChoiceField(queryset=Icon.objects.all(), widget=IconChooser,
            label=_("Which icon do you like best?"))
    class Meta:
        model = Profile
        fields = ('name', 'source_icon', 'icon_color')

class EmailForm(forms.Form):
    email = forms.EmailField(required=True)

    def clean_email(self):
        if self.user.email != self.cleaned_data['email']:
            try:
                User.objects.get(email=self.cleaned_data['email'])
                raise ValidationError("That email address is already in use.")
            except User.DoesNotExist:
                pass
            try:
                ecr = EmailChangeRequest.objects.get(email=self.cleaned_data['email'])
                if ecr.user != self.user:
                    raise ValidationError("That email address is already in use.")
            except EmailChangeRequest.DoesNotExist:
                pass

        return self.cleaned_data['email']

