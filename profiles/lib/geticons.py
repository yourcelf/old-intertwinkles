import os
import sys
import glob
import json
import shutil
import tempfile
import subprocess
from collections import defaultdict
from PIL import Image, ImageDraw

ICON_SOURCE = os.path.expanduser("~/git/nounscraper/icons/")
def icon_path(icon, size='large'):
    suffix = {
            'large': '100.png',
            'medium': '32.png',
            'small': '16.png',
            'attribution': 'attribution.html',
    }[size]
    return os.path.join(
        ICON_SOURCE,
        "%s-%s-%s" % (
            icon['noun'].lower().replace(" ", "-"),
            icon['id'].replace("icon-", ""),
            suffix
        ),
    )


def load_existing():
    with open("icon-defs.json") as fh:
        defs = json.load(fh)
    return defs

def copy_known():
    with open("icon-defs.json") as fh:
        defs = json.load(fh)
    for icon_id, icon in defs.items():
        for size, path in icon['filename'].items():
            shutil.copy(
                os.path.join(ICON_SOURCE, path),
                os.path.join("../assets/img/icons", path)
            )

def load_all():
    icons = defaultdict(list)
    for filename in glob.glob(os.path.join(ICON_SOURCE, "*.json")):
        with open(filename) as fh:
            icon_def = json.load(fh)
            icons[icon_def['noun']].append(icon_def)
    return icons

def choose_icon(icons):
    # Create something to look at.
    imgs = [icon_path(icon) for icon in icons]
    img = Image.new('RGBA', (100 * len(icons), 120))
    for i, path in enumerate(imgs):
        sub = Image.open(path)
        if sub.size != (100, 100):
            print "OH NOES!  %s is %s, skipping" % (path, sub.size)
            continue
        img.paste(Image.open(path), (100 * i, 0, 100 * (i + 1), 100))
    draw = ImageDraw.Draw(img)
    ids = [icon['id'] for icon in icons]
    for i in range(len(ids)):
        draw.text((100 * i, 102), str(i + 1) + " " + icon['noun'])
    with tempfile.NamedTemporaryFile(delete=False, suffix=".png") as fh:
        name = fh.name
    img.save(name, "png")
    proc = subprocess.Popen(["display", name])

    # Ask which one to use.
    while True:
        print icons[0]['noun'], range(1, len(ids) + 1)
        choice = raw_input("Choice: ")
        if choice == "0":
            print "Ignoring."
            icon = None
            break
        try:
            icon = icons[int(choice) - 1]
            print icon['id']
            break
        except (ValueError, IndexError):
            print "Please choose one of these IDs:"
            continue
    del draw
    os.remove(name)
    proc.kill()
    return icon

def load_blacklist():
    with open("blacklist.json") as fh:
        return json.load(fh)

def load_unknown():
    known = load_existing()
    known_nouns = set(icon['noun'] for id_,icon in known.iteritems())
    all_icons = load_all()
    blacklist = set(load_blacklist())
    try:
        for noun, icons in all_icons.iteritems():
            if noun in known_nouns or noun in blacklist:
                continue
            icon = choose_icon(icons)
            if icon is None:
                blacklist.add(noun)
                continue
            with open(icon_path(icon, "attribution")) as fh:
                attribution = fh.read()
            known[icon['id']] = {
                'noun': noun,
                'attribution': attribution,
                'filename': {
                    'small': os.path.basename(icon_path(icon, 'small')),
                    'medium': os.path.basename(icon_path(icon, 'medium')),
                    'large': os.path.basename(icon_path(icon, 'large')),
                }
            }
    except KeyboardInterrupt:
        pass
    finally:
        with open("icon-defs.json", 'w') as fh:
            json.dump(known, fh, indent=4)
        with open("blacklist.json", 'w') as fh:
            json.dump(list(blacklist), fh)

def _fix_old_defs():
    all_icons = load_all()
    known = load_existing()
    for noun, icons in all_icons.iteritems():
        for icon in icons:
            if icon['id'] in known:
                known[icon['id']]['noun'] = noun
                known[icon['id']]['svg'] = '<?xml version="1.0" encoding="utf-8"?>\n<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.0//EN" "http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd">\n' + icon['svg']
                for size, fname in known[icon['id']]['filename'].items():
                    known[icon['id']]['filename'][size] = fname.replace(" ", "-")
    with open("icon-defs.json", 'w') as fh:
        json.dump(known, fh, indent=4)

if __name__ == "__main__":
    if len(sys.argv) > 1:
        if sys.argv[1] == "copy":
            copy_known()
        elif sys.argv[1] == "fix":
            _fix_old_defs()
    else:
        load_unknown()
