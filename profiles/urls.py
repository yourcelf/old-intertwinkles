from django.conf.urls.defaults import include, patterns, url

urlpatterns = patterns('profiles.views',
    url(r'^signin/$', 'signin', name='profiles_signin'),
    url(r'^signin/invite$', 'signin_fail', name='profiles_signin_fail'),
    url(r'^signout/$', 'signout', name='profiles_signout'),
    url(r'^icon_attribution/$', 'icon_attribution', name='profiles_icon_attribution'),
    url(r'^post_signin/$', 'post_signin', name='profiles_post_signin'),
    url(r'^edit/$', 'edit_profile', name='profiles_edit_profile'),
    url(r'^new/$', 'create_profile', name='profiles_create_profile'),
    url(r'^messages/$', 'view_messages', name='notification_notices'),
)
