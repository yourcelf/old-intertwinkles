import re
import os
import json
import random
from django.db import models
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from django.conf import settings
from django.utils.safestring import mark_safe
from django.template.defaultfilters import escape

from django_browserid.auth import default_username_algo
from notification.carriers import MOBILE_CARRIERS
from jsonfield import JSONField

class Icon(models.Model):
    source_id = models.CharField(max_length=20)
    name = models.CharField(max_length=50)
    svg = models.TextField()
    attribution = models.TextField()

    def __unicode__(self):
        return self.name

    def get_path(self, color, size):
        return os.path.join(
            settings.MEDIA_ROOT,
            getattr(settings, "PROFILE_ICON_PREFIX", "profile_icons",),
            "user",
            "{0}-{1}-{2}-{3}.png".format(
                self.source_id,
                self.name,
                color,
                size
            )
        )

class EmailChangeRequest(models.Model):
    user = models.ForeignKey(User, unique=True)
    email = models.EmailField(unique=True)

    def clean(self):
        if self.user.email == self.email:
            raise ValidationError("That's not a different email address.")

    def __unicode__(self):
        return "%s" % self.email

class Profile(models.Model):
    SIZES = [16, 32, 64, 128]
    user = models.OneToOneField(User)
    name = models.CharField(max_length=255)
    source_icon = models.ForeignKey(Icon, null=True, blank=True)
    icon_color = models.CharField(max_length=6, default="000000")
    icon_cache = JSONField(default="[]")
    mobile_number = models.CharField(max_length=14, blank=True)
    mobile_carrier = models.CharField(max_length=32, blank=True,
        choices=((a, a) for a in sorted(MOBILE_CARRIERS.keys())),
    )

    def set_email(self, email):
        self.user.email = email
        username_algo = getattr(settings, 'BROWSERID_USERNAME_ALGO',
                default_username_algo)
        self.user.username = username_algo(email)
        self.user.save()
        return self.user

    def clean(self):
        self.icon_color = self.icon_color.upper()
        self.mobile_number = re.sub('[^0-9]', '', self.mobile_number)
        if self.mobile_number or self.mobile_carrier:
            if not self.mobile_number:
                raise ValidationError(
                        "Mobile carrier specified, but missing mobile number.")
            if not self.mobile_carrier:
                raise ValidationError(
                        "Mobile number specified, but missing mobile carrier.")

        if not re.match("[A-F0-9]{6}", self.icon_color):
            raise ValidationError("Invalid color '%s'." % self.icon_color)
        if self.pk:
            orig = Profile.objects.get(pk=self.pk)
            if orig.icon_color != self.icon_color or orig.source_icon_id != self.source_icon_id:
                self.icon_cache = None
        if not self.source_icon:
            self.choose_random_icon_and_color()
        if not self.icon_cache:
            self.render_icons()

    def render_icons(self):
        from profiles.tasks import render_icon
        self.icon_cache = render_icon.delay(
            self.source_icon_id, self.icon_color, self.SIZES
        ).get()

    def __unicode__(self):
        return self.name

    def format_large_icon(self):  return self.format_icon("128")
    def format_medium_icon(self): return self.format_icon("64")
    def format_small_icon(self):  return self.format_icon("32")
    def format_tiny_icon(self):   return self.format_icon("16")
    def format_icon(self, size):
        return mark_safe("<img src='{0}' alt='{1}' title='{2}' />".format(
            escape(self.get_icon_url(size)),
            escape(self.get_icon_name()),
            escape(self.name),
        ))
    def get_icon_url(self, size='32'):
        return "/".join((settings.MEDIA_URL, self.icon_cache[size]))

    def get_icon_name(self):
        return self.source_icon.name

    def choose_random_icon_and_color(self):
        count = Icon.objects.all().count()
        self.source_icon = Icon.objects.all()[random.randint(0, count-1)]
        self.icon_color = "%02x%02x%02x" % (
            random.randint(0, 255),
            random.randint(0, 255),
            random.randint(0, 255)
        )
