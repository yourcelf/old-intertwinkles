# -*- coding: utf-8 -*-
import os
from django.contrib.auth import logout, login
from django.contrib import messages
from django.shortcuts import redirect, render
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage

from profiles.models import Profile, EmailChangeRequest
from profiles.forms import ProfileForm, NewProfileForm, EmailForm
from notification.models import Notice, NoticeType, NOTICE_MEDIA, \
                                get_notification_setting

def signout(request):
    if request.user.is_authenticated(): 
        logout(request)
        messages.info(request, _("Signed out."))
        return render(request, "profiles/signout.html", {
            'suppress_browserid': True
        })
    return redirect("home")

@login_required
def post_signin(request):
    try:
        # Do we have an email change request to this user?
        emailreq = EmailChangeRequest.objects.get(email=request.user.email)
        # If an emailreq exists, it means that this email hasn't signed in
        # before, and someone had requested a change of email address to this
        # address.  The fact that we're here is our "confirmation" that the email
        # address is legit.  But we don't want the new user.  Delete it, confirm
        # the EmailChangeRequest, and authenticate the old user with new address.
        new_user = request.user
        logout(request)
        new_user.delete()
        # Be careful here with db lookups/caching, so we don't keep old
        # versions of user model around..
        orig_user = emailreq.user.profile.set_email(emailreq.email)
        # HACK: pretend we just came from BrowserID.
        orig_user.backend = 'django_browserid.auth.BrowserIDBackend'
        login(request, orig_user)
        EmailChangeRequest.objects.filter(email=emailreq.email).delete()
        messages.success(request, "Email updated.")
    except EmailChangeRequest.DoesNotExist:
        pass
    try:
        request.user.get_profile()
    except Profile.DoesNotExist:
        return redirect("profiles_create_profile")
    return redirect("home")

@login_required
def create_profile(request):
    try:
        profile = request.user.get_profile()
    except Profile.DoesNotExist:
        profile = Profile(user=request.user)
    profile_form = NewProfileForm(request.POST or None, instance=profile)
    if profile_form.is_valid():
        try:
            profile = profile_form.save()
        except Exception:
            from django.db import connection
            raise Exception(unicode(connection.queries))
        return redirect(request.GET.get("next", "groups_manage_groups"))
    return render(request, "profiles/new_profile.html", {
        'form': profile_form,
    })

@login_required
def edit_profile(request):
    try:
        profile = request.user.get_profile()
    except Profile.DoesNotExist:
        return redirect("profiles_create_profile")
    profile_form = ProfileForm(request.POST or None, instance=profile)

    try:
        emailreq = EmailChangeRequest.objects.get(user=request.user)
    except EmailChangeRequest.DoesNotExist:
        emailreq = EmailChangeRequest(user=request.user, email=request.user.email)
    email_form = EmailForm(request.POST or None, initial={
        'email': emailreq.email
    })
    email_form.user = request.user

    # Notice settings
    notice_types = NoticeType.objects.all()
    settings_table = []
    for notice_type in notice_types:
        settings_row = []
        for medium_id, medium_display in NOTICE_MEDIA:
            form_label = "%s_%s" % (notice_type.label, medium_id)
            setting = get_notification_setting(request.user, notice_type, medium_id)
            if request.method == "POST":
                if request.POST.get(form_label) == "on":
                    if not setting.send:
                        setting.send = True
                        setting.save()
                else:
                    if setting.send:
                        setting.send = False
                        setting.save()
            settings_row.append((form_label, setting.send, medium_display))
        settings_table.append({"notice_type": notice_type, "cells": settings_row})
    
    # Profile and email settings
    if profile_form.is_valid() and email_form.is_valid():
        profile_form.save()
        if email_form.cleaned_data['email'] != request.user.email:
            emailreq.email = email_form.cleaned_data['email']
            emailreq.save()
            logout(request)
            messages.info(request, _("Please sign in again as '%(email)s' to confirm your change of email. Go ahead and click that big blue 'Sign in' button below.") % {'email': emailreq.email})
            return redirect("profiles_signin")
        else:
            EmailChangeRequest.objects.filter(user=request.user).delete()
            messages.success(request, _("Profile saved."))
            return redirect("/")
    return render(request, "profiles/edit_profile.html", {
        'profile_form': profile_form,
        'email_form': email_form,
        'emailreq': emailreq,
        'notice_types': notice_types,
        'notice_settings_table': settings_table,
        'from_email': settings.DEFAULT_FROM_EMAIL,
    })

@login_required
def view_messages(request):
    notices = Notice.objects.notices_for(request.user, on_site=True)
    paginator = Paginator(notices, 32)
    try:
        page_num = int(request.GET.get('page', 1))
    except ValueError:
        page_num = 1

    try:
        page = paginator.page(request.GET.get('page'))
    except PageNotAnInteger:
        page = paginator.page(1)
    except EmptyPage:
        page = paginator.page(paginator.num_pages)

    return render(request, "profiles/messages.html", {
        'notices_page': page,
    })

def signin_fail(request):
    if settings.BROWSERID_CREATE_USER == False:
        messages.error(request,
            u"""Only invited users can sign in during beta.
                Request an invitation down there: ↓↓↓"""
        )
        form = EmailForm(request.POST or None)
        if form.is_valid():
            emsu, created = EmailSignup.objects.get_or_create(
                    email=form.cleaned_data['email']
            )
            if created:
                mail_managers(
                    subject="New InterTwinkles email signup",
                    message="New signup from: %s.\n%s" % (
                        emsu.email,
                        "%s%s" % (
                            settings.SITE_URL,
                            "/admin/base/emailsignup/%s" % emsu.pk
                        )
                    )
                )
                messages.info(request,
                        "Thanks!  We hope to get in touch with you soon."
                )
                return redirect("home")
        return render(request, "profiles/profiles_invite.html", {
            'form': form,
            'suppress_browserid': True
        })
    else:
        messages.error(request, u"Something went wrong there.  Try again?")
        return redirect("profiles_signin")

def signin(request):
    return render(request, "profiles/signin.html", {
        'login_page': True,
        'next': request.GET.get(REDIRECT_FIELD_NAME, None),
        'REDIRECT_FIELD_NAME': REDIRECT_FIELD_NAME, 
        'suppress_browserid': True,
    })

def icon_attribution(request):
    with open(os.path.join(settings.MEDIA_ROOT,
            "profile_icons",
            "attribution.json")) as fh:
        attribution_json = fh.read()

    return render(request, "profiles/icon_attribution.html", {
        'attribution_json': attribution_json
    })

