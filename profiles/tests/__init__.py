"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

import os, time
from urllib import urlencode

from django.test import TestCase, LiveServerTestCase
from django.conf import settings
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.test.utils import override_settings

from PIL import Image, ImageChops
from django_browserid.tests import mock_browserid
from django_browserid import get_audience
from bs4 import BeautifulSoup

from profiles.models import Icon, Profile, EmailChangeRequest
from profiles.tasks import render_icon

@override_settings(CELERY_ALWAYS_EAGER=True)
class TestIcons(TestCase):
    def test_icon_rendering(self):
        """
        Tests that 1 + 1 always equals 2.
        """
        icon = Icon.objects.get(source_id='icon-2244')
        paths = render_icon.delay(icon.pk, "00FF00", [16]).get()
        # HACK: can't seem to get imagemagick to block.
        time.sleep(0.1)
        reference = Image.open(os.path.join(
                os.path.dirname(__file__),
                "icon-2244-Oven-00FF00-16.png"))
        test_img = Image.open(os.path.join(settings.MEDIA_ROOT, paths['16']))
        self.assertTrue(
                ImageChops.difference(reference, test_img).getbbox() is None
        )

@override_settings(CELERY_ALWAYS_EAGER=True)
class TestBrowserID(TestCase):
    def login(self, email):
        with mock_browserid(email):
            self.assertTrue(
               self.client.login(assertion='one', audience="two")
            )

    def create_user_and_profile(self, username, email):
        with self.settings(CELERY_ALWAYS_EAGER=True):
            u = User.objects.create(username=username, email=email)
            profile = Profile.objects.create(name=username, user=u)
            profile.full_clean()
            profile.save()
            self.assertNotEqual(u.profile.source_icon, None)
            return u

    def test_change_email(self):
        user = self.create_user_and_profile("test", "test@example.com")

        self.login("test@example.com")
        res = self.client.post(reverse("profiles_edit_profile"), {
            'name': 'test',
            'email': 'new@example.com',
            'source_icon': user.profile.source_icon.pk,
            'icon_color': user.profile.icon_color,
        }, follow=True)
        self.assertRedirects(res, reverse("profiles_signin"))
        self.assertTrue("new@example.com" in res.content)

        # An email change request now exists ...
        self.assertEquals(
                len(EmailChangeRequest.objects.filter(
                    email="new@example.com", user=user
                )), 1)
        # ... but the email hasn't yet been changed.
        self.assertEquals(User.objects.get(pk=user.pk).email, "test@example.com")

        # We're not logged in anymore; we've been prompted to login with the new email.
        url = reverse("profiles_edit_profile")
        self.assertRedirects(
                self.client.get(url, follow=True),
                reverse("profiles_signin") + "?{0}".format(urlencode({'next': url}))
        )

        # If we log in again with the old address, on profile edit, we see
        # a message that shows the old address is unconfirmed.
        self.login("test@example.com")
        res = self.client.get(reverse("profiles_edit_profile"))
        self.assertTrue("This address is unconfirmed" in res.content)
        self.assertTrue("test@example.com" in res.content)
        soup = BeautifulSoup(res.content)
        self.assertEquals(soup.find(id="id_email").get("value"),
                          "new@example.com")
        
        # But if we log in as the new address, and hit the 'post_signin'
        # page, the address is changed.
        self.client.logout()
        self.login("new@example.com")
        res = self.client.get(reverse("profiles_post_signin"), follow=True)
        res = self.client.get(reverse("profiles_edit_profile"), follow=True)
        self.assertFalse("This address is unconfirmed" in res.content)
        soup = BeautifulSoup(res.content)
        self.assertEquals(soup.find(id="id_email").get("value"),
                          "new@example.com")
        self.assertEquals(
            len(EmailChangeRequest.objects.filter(email="new@example.com")),
            0
        )
        self.assertEquals(
            len(EmailChangeRequest.objects.filter(user=user)),
            0
        )
        self.assertEquals(
            len(User.objects.filter(email="test@example.com")), 0)
        self.assertEquals(
            len(User.objects.filter(email="new@example.com")), 1)

    def _change_profile(self, user, attrs):
        self.login(user.email)
        post_data = {
            'name': user.profile.name,
            'email': user.email,
            'source_icon': user.profile.source_icon.pk,
            'icon_color': user.profile.icon_color,
        }
        post_data.update(attrs)
        return self.client.post(
                reverse("profiles_edit_profile"),
                post_data, follow=True)

    def test_existing_addresses(self):
        u1 = self.create_user_and_profile("u1", "one@example.com")
        u2 = self.create_user_and_profile("u2", "two@example.com")

        # If you try to change your address to one another user has, you get
        # rejected.
        res = self._change_profile(u2, {'email': u1.email})
        self.assertEquals(res.status_code, 200)
        self.assertTrue(
            "That email address is already in use" in res.content
        )
        self.assertEquals(EmailChangeRequest.objects.count(), 0)

        # Try a new address.  Address should be accepted; but will still be
        # uncoonfirmed.
        res = self._change_profile(u2, {'email': "new@example.com"})
        self.assertRedirects(res, reverse("profiles_signin"))
        self.assertEquals(User.objects.get(pk=u2.pk).email, "two@example.com")
        self.assertEquals(
                EmailChangeRequest.objects.get(email='new@example.com').user, 
                u2)

        # Meanwhile, the second user tries to change to the same unconfirmed
        # address.  The address shows as not valid.
        res = self._change_profile(u1, {'email': "new@example.com"})
        self.assertEquals(res.status_code, 200)
        self.assertTrue(
            "That email address is already in use" in res.content
        )
        self.assertEquals(EmailChangeRequest.objects.count(), 1)
