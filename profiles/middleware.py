from django.conf import settings
from django.shortcuts import redirect
from django.core.urlresolvers import reverse, NoReverseMatch
from urllib import urlencode

from profiles.models import Profile

class DemandProfile(object):
    def process_request(self, request):
        try:
            create_url = reverse("profiles_create_profile")
        except NoReverseMatch:
            return None
        # Allow media/static URL through, as well as logout, and
        # login_redirect_url (which processes email change requests).  All
        # others: ensure they have a profile icon set before proceding.
        if request.user.is_authenticated() and \
                request.method == 'GET' and \
                not request.path.startswith(settings.MEDIA_URL) and \
                not request.path.startswith(settings.STATIC_URL) and \
                not request.path.startswith(settings.LOGIN_REDIRECT_URL) and \
                not request.path.startswith(settings.LOGOUT_URL) and \
                request.path != create_url:
            try:
                assert request.user.profile.source_icon != None
            except (Profile.DoesNotExist, AssertionError):
                return redirect("?".join((
                    create_url,
                    urlencode({'next': request.path})
                )))
        return None
