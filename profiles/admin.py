from django.contrib import admin

from profiles.models import Profile, Icon

admin.site.register(Profile)
admin.site.register(Icon)
