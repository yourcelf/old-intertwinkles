import os
import re
import tempfile
import subprocess
import xml.parsers.expat

from django.conf import settings
from celery.task import task

from profiles.models import Icon

class ViewBoxGetter(object):
    def start_element(self, name, attrs):
        if name == "svg":
            self.view_box = attrs['viewbox']

def _get_viewbox(icon):
    p = xml.parsers.expat.ParserCreate()
    vbg = ViewBoxGetter()
    p.StartElementHandler = vbg.start_element
    p.Parse(icon.svg)
    return (float(a) for a in re.split("[,\s]", vbg.view_box))

def _icon_dpi(size, icon):
    x1,y1,x2,y2 = _get_viewbox(icon)
    width = x2-x1
    height = y2-y1
    # fallback for invalid viewbox
    return 90. / max(width, height) * size

@task
def render_icon(icon_id, color, sizes, dests=None):
    icon = Icon.objects.get(pk=icon_id)

    # Write temporary SVG file.
    with tempfile.NamedTemporaryFile(delete=False, suffix=".svg") as fh:
        svg_name = fh.name
        fh.write(icon.svg)

    abs_paths = {}
    rel_paths = {}
    procs = []

    dests = dests or [None for i in range(len(sizes))]
    
    # Rasterize svg.
    for size, path in zip(sizes, dests):
        abs_paths[size] = path or icon.get_path(color, size)
        if not os.path.exists(os.path.dirname(abs_paths[size])):
            os.makedirs(os.path.dirname(abs_paths[size]))
        rel_paths[size] = os.path.relpath(abs_paths[size], settings.MEDIA_ROOT)
        # Can't use the --command=arg format when not using a shell here, so we
        # use the abbreviated "-c arg" forms.
        proc = subprocess.Popen(["inkscape",
            '-e', abs_paths[size], # --export-png
            "-d", str(_icon_dpi(size, icon)), # --export-dpi
            "--export-area-drawing",
            "-y", "0", # --export-background-opacity
            "--without-gui",
            svg_name], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        procs.append(proc)
    for proc in procs:
        proc.communicate()

    # Set colors and normalize size
    procs = []
    for size, path in abs_paths.items():
        proc = subprocess.Popen(["convert",
            path,
            "+level-colors",
            "#{0},".format(color),
            "-gravity",
            "center",
            "-background",
            "transparent",
            "-extent",
            "{0}x{0}".format(size, size),
            path,
        ])
    for proc in procs:
        proc.communicate()

    # Clean up
    os.remove(svg_name)
    rel_paths = dict((str(k), v) for k,v in rel_paths.items())
    return rel_paths
