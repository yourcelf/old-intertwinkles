import os
import json
from django.core.management import BaseCommand

from profiles.models import Icon

BASE = os.path.dirname(__file__)

class Command(BaseCommand):
    args = "[icon-defs-file (default ./lib/icon-defs.json)]"
    def handle(self, *args, **kwargs):
        if args:
            defs_file = args[0]
        else:
            defs_file = os.path.join(BASE, "..", "..", "lib", "icon-defs.json")
        with open(defs_file) as fh:
            defs = json.load(fh)
        total = 0
        new = 0
        for icon_id, icon in defs.iteritems():
            icon, created = Icon.objects.get_or_create(
                    source_id=icon_id,
                    name=icon['noun'],
                    svg=icon['svg'],
                    attribution=icon['attribution'],
            )
            total += 1
            if created:
                new += 1
        print "{0} icons read, {1} new".format(total, new)
