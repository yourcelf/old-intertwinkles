import os
import json
import subprocess

from django.core.management import BaseCommand
from django.conf import settings

from profiles.models import Icon
from profiles.tasks import render_icon

class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        images = []
        desc = []
        attrib = []
        for icon in Icon.objects.all():
            path = icon.get_path("000000", 32)
            # Flip over to a separate dir to keep these out of the user icons.
            path = os.path.join(
                os.path.dirname(path), "..", "choices",
                os.path.basename(path)
            )
            if not os.path.exists(path):
                result = render_icon(icon.pk, "000000", [32], [path])
            images.append(path)
            desc.append({
                'pk': icon.pk,
                'name': icon.name
            })
            attrib.append({
                'id': icon.source_id,
                'name': icon.name,
                'attribution': icon.attribution,
            })

        proc = subprocess.Popen(["montage",
                "-mode", "concatenate",
                "-background", "transparent",
                "-tile", "{0}x1".format(len(images))
            ] + images + [
                os.path.join(settings.MEDIA_ROOT, "profile_icons", "chooser.png")
            ])

        with open(os.path.join(settings.MEDIA_ROOT,
                "profile_icons",
                "chooser.json"), 'w') as fh:
            json.dump(desc, fh)
        with open(os.path.join(settings.MEDIA_ROOT,
                "profile_icons",
                "attribution.json"), 'w') as fh:
            json.dump(attrib, fh)
