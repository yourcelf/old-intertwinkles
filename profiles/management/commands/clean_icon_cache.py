import glob
import os

from django.core.management import BaseCommand
from django.conf import settings

from profiles.models import Profile

class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        needed = set()
        rerender = []
        for profile in Profile.objects.all():
            for size in Profile.SIZES:
                path = profile.source_icon.get_path(profile.icon_color, size)
                if not os.path.exists(path):
                    profile.render_icons()
                    profile.save()
                needed.add(path)

        existing = set(glob.glob(os.path.join(settings.MEDIA_ROOT,
            "profile_icons", 
            "user",
            "icon*.png")))
        extraneous = existing ^ needed
        count = len(extraneous)
        for path in extraneous:
            os.remove(path)
        print "Removed {0} extraneous files.".format(count)
