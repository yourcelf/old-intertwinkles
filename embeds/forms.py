from django import forms

from embeds.models import Embed

class EmbedForm(forms.ModelForm):
    class Meta:
        model = Embed
        fields = ['name', 'type']
