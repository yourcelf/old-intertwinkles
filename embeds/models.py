import json
import string
import random
import requests
from urlparse import urlparse
from django.db import models
from django.utils.timezone import now
from django.core.exceptions import ValidationError
from django.core.validators import URLValidator
from django.core.urlresolvers import reverse
from django.conf import settings
from django.contrib.auth.models import User
from model_utils.managers import PassThroughManager
from django.template.defaultfilters import slugify

from groups.models import Group, PQ, GroupQuerySet

class EmbedQuerySet(GroupQuerySet):
    class MQ(PQ):
        accessor = "group__membership"

    def current(self):
        return self.filter(archived=False)

    def archived(self):
        return self.filter(archived=True)

    def etherpads(self):
        return self.filter(type='etherpad')

    def dotstorms(self):
        return self.filter(type='dotstorm')



EMBED_TYPES = ((
    ('etherpad', 'Etherpad'),
    ('dotstorm', 'Dotstorm'),
))

class Embed(models.Model):
    group = models.ForeignKey(Group)
    name = models.CharField(max_length=255)
    url  = models.TextField(validators=[URLValidator(verify_exists=False)])
    type = models.CharField(choices=EMBED_TYPES, max_length=25)
    created = models.DateTimeField(default=now)
    modified = models.DateTimeField(default=now)
    archived = models.BooleanField()

    objects = PassThroughManager.for_queryset_class(EmbedQuerySet)()

    def touch(self):
        self.modified = now()
        self.save()

    def clean(self):
        if not self.url:
            if self.type not in settings.EMBED_SERVERS:
                raise ValidationError("Unknown embed type '%s'" % self.type)
            base_url = settings.EMBED_SERVERS[self.type]
            if self.type in ("dotstorm", "etherpad"):
                self.url = "%s%s%s" % (
                        base_url, self.random_prefix(),
                        slugify(self.name)[:50]
                )
            else:
                raise NotImplementedError("Unhandled type '%s'" % self.type)

    def get_text(self):
        """
        Make an HTTP request to retrieve the full text of this document.  Note
        that this is a very expensive operation.
        """
        if self.type == 'etherpad':
            res = requests.get(self.url.rstrip("/") + "/export/txt")
            return res.text
        elif self.type == 'dotstorm':
            res = requests.get(self.url.rstrip("/") + "/json/")
            data = json.loads(res.text)
            parts = [data.get('name', ''), data.get('topic', '')]
            for group in data.get('groups', []):
                parts.append(group.get('label', ''))
                for idea in group.get('ideas', []):
                    parts.append(" / " + idea.get('description', ''))
                    if idea.get('tags'):
                        parts.append("(%s)" % ", ".join(idea.get('tags', '')))
                    parts.append('\n')
                parts.append('\n')
            return "\n".join(parts)

    def get_absolute_url(self):
        return reverse("embeds:show", args=[self.pk])

    def get_group_name(self):
        return self.group.name

    def random_prefix(self):
        options = string.ascii_letters + string.digits
        return "".join(random.choice(options) for i in range(16))

    def __unicode__(self):
        return self.url

    class Meta:
        ordering = ['-created']
        get_latest_by = 'created'
        unique_together = ['name', 'group']

class EmbedVisitQuerySet(GroupQuerySet):
    class MQ(PQ):
        accessor = 'embed__group__membership'

class EmbedVisit(models.Model):
    user = models.ForeignKey(User)
    embed = models.ForeignKey(Embed)
    created = models.DateTimeField(default=now)

    objects = PassThroughManager.for_queryset_class(EmbedVisitQuerySet)()

    class Meta:
        ordering = ['-created']
        get_latest_by = 'created'
