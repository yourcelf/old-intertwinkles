import json
from datetime import timedelta
from django.shortcuts import render, redirect
from django.db.models import F
from django.template import loader, Context
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.exceptions import PermissionDenied
from django.http import Http404, HttpResponse, HttpResponseBadRequest
from django.views.generic import View
from django.utils.timezone import now

from groups.models import Group
from embeds.models import Embed, EmbedVisit
from embeds.forms import EmbedForm
from timelines.models import Event
from intertwinkles.base.utils import json_dt_handler
from profiles.utils import user_public_dict

def _paginate(request, qs, count=25):
    paginator = Paginator(qs, count)
    page = request.GET.get('page')
    try:
        objects = paginator.page(page)
    except PageNotAnInteger:
        objects = paginator.page(1)
    except EmptyPage:
        raise Http404
    return objects

@login_required
def list_embeds(request, group_id):
    try:
        group = Group.objects.active_for(request.user).get(pk=group_id)
    except Group.DoesNotExist:
        raise Http404
    
    qs = group.embed_set.current()
    page = request.GET.get('page')
    embeds = _paginate(request, qs)
    return render(request, "embeds/list_embeds.html", {
        'group': group,
        'membership': group.membership_set.current_for(request.user).get(),
        'embeds': embeds,
    })

@login_required
def touch_embed(request, embed_id):
    if request.method == 'POST':
        try:
            Embed.objects.active_for(request.user).get(pk=embed_id).touch()
        except Embed.DoesNotExist:
            raise PermissionDenied
        response = HttpResponse('{"result": "success"}')
        response['Content-type'] = "application/json"
        return response
    return HttpResponseBadRequest('{"error": "POST required"}')

@login_required
def show_embed(request, embed_id):
    try:
        embed = Embed.objects.active_for(request.user).get(pk=embed_id)
    except Embed.DoesNotExist:
        raise Http404
    group = embed.group

    # Recent visits
    try:
        embed.embedvisit_set.filter(user=request.user, 
            created__gte=now() - timedelta(seconds=60 * 5)
        )[0]
    except IndexError:
        embed.embedvisit_set.create(user=request.user)

    
    key = Event.objects.get_key(embed, "visit")
    if not embed.archived:
        Event.objects.touch(
                match={
                    'user': request.user,
                    'group': group,
                    'key': key,
                    'date__gte': now() - timedelta(seconds=60*5),
                },
                update={
                    'date': now(),
                    'user': request.user,
                    'group': embed.group,
                    'key': key,
                    'data': {
                        'type': 'embed_visit',
                        'title': 'Visit',
                        'content': loader.get_template(
                                "timelines/events/embed_visit.html"
                            ).render(Context({'user': request.user})),
                    }
                })
    
    people = {}
    events = []
    for e in Event.objects.all():
        if e.user_id not in people:
            people[e.user_id] = user_public_dict(e.user)
        events.append(e.to_dict())

    events = json.dumps([e.to_dict() for e in Event.objects.involving_model(embed)],
            default=json_dt_handler)
    never_visited = group.membership_set.exclude(
            user__event__key=key,
            user__event__group=group)

    return render(request, "embeds/show_embed.html", {
        'group': embed.group,
        'membership': group.membership_set.current_for(request.user).get(),
        'embed': embed,
        'timelineData': events,
        'peopleData': json.dumps(people, default=json_dt_handler),
        'never_visited': never_visited,
    })

@login_required
def new_embed(request, group_id):
    try:
        group = Group.objects.active_for(request.user).get(pk=group_id)
    except Group.DoesNotExist:
        raise Http404

    form = EmbedForm(request.POST or None)
    if form.is_valid():
        embed = form.save(commit=False)
        embed.group = group
        embed.clean()
        embed.save()
        return redirect(embed.get_absolute_url())
    return render(request, "embeds/new_embed.html", {
        'group': group,
        'membership': group.membership_set.current_for(request.user).get(),
        'form': form,
        'embed': Embed(group=group),
    })

@login_required
def archive_embed(request, embed_id):
    pass
