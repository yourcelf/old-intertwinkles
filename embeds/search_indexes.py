from haystack import indexes
from embeds.models import Embed

class EmbedIndex(indexes.SearchIndex, indexes.Indexable):
    text  = indexes.CharField(document=True, use_template=True)
    title = indexes.CharField(model_attr='name')
    group_name = indexes.CharField(model_attr='get_group_name')
    type  = indexes.CharField(model_attr='type')
    group_id = indexes.IntegerField(model_attr='group_id')
    created = indexes.DateTimeField(model_attr='created')
    modified = indexes.DateTimeField(model_attr='modified')
    archived = indexes.BooleanField(model_attr='archived')
    absolute_url = indexes.CharField(model_attr='get_absolute_url')

    def get_model(self):
        return Embed

    def get_updated_field(self):
        return 'modified'
