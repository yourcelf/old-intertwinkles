from django.conf.urls.defaults import include, patterns, url

urlpatterns = patterns('embeds.views',
    url(r'^(?P<group_id>\d+)/$', 'list_embeds', name='list'),
    url(r'^(?P<group_id>\d+)/new$', 'new_embed', name='new'),
    url(r'^doc/(?P<embed_id>\d+)/$', 'show_embed', name='show'),
    url(r'^doc/(?P<embed_id>\d+)/archive$', 'archive_embed', name='archive'),
    url(r'^doc/(?P<embed_id>\d+)/touch$', 'touch_embed', name='touch'),
)
