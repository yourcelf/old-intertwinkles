from django.http import Http404
from django.shortcuts import redirect

from shorty.models import expand
from shorty import tasks

def expand_url(request, short_code):
    long_url = expand(short_code)
    if long_url is None:
        raise Http404
    tasks.process_events.delay(url=long_url)
    return redirect(long_url)
