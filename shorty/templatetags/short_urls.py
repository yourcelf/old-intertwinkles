from django import template
from django.template.defaultfilters import stringfilter
from shorty.models import shorten as shorty_shorten
from shorty.models import expand as shorty_expand

register = template.Library()

@register.filter
@stringfilter
def shorten(value, name=None):
    return shorty_shorten(value, name)

@register.filter
@stringfilter
def expand(value):
    return shorty_expand(value)
