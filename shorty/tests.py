from urlparse import urlparse
from django.test import TestCase
from django.core.urlresolvers import resolve, reverse
from django.core.cache import cache

from shorty.models import ShortURL, SHORT_URL_LENGTH, shorten, expand
from shorty.views import expand_url as views_expand_url
from django.template import Template, Context

class TestCreateShortURL(TestCase):
    def setUp(self):
        cache.clear()

    def test_create_short_url(self):
        one = ShortURL.objects.shorten("http://eff.org")
        two = ShortURL.objects.shorten("http://aclu.org")
        for short in [one, two]:
            self.assertEquals(len(short.short_code), SHORT_URL_LENGTH)
            path = urlparse(short.format_short_url()).path
            match = resolve(path)
            self.assertEquals(match.kwargs, {'short_code': short.short_code})
            self.assertEquals(match.func, views_expand_url)
        # Omitting domain
        ex = ShortURL.objects.shorten("/stuff")
        self.assertTrue(ex.long_url.startswith("http://example.com/"))
        with self.settings(DEFAULT_HTTP_PROTOCOL="https"):
            self.assertTrue(ex.format_short_url().startswith("https://example.com"))
        self.assertEquals(ex.format_short_url(False),
                reverse("shorty_expand", args=[ex.short_code]))

    def test_non_unique_long_urls(self):
        one = ShortURL.objects.shorten("http://eff.org")
        # Just grabbing a new short URL doesn't create a new one.
        self.assertEquals(one, ShortURL.objects.shorten("http://eff.org"))
        # But specifying a custom short code does.
        eff = ShortURL.objects.shorten("http://eff.org", "eff")
        self.assertNotEquals(one, eff)

        # Which one we get when we have two is indeterminate.
        three = ShortURL.objects.shorten("http://eff.org")
        self.assertTrue(
            three.pk == one.pk or three.pk == eff.pk
        )

    def test_named_short_url(self):
        one = ShortURL.objects.shorten("http://eff.org", "eff")
        self.assertEquals(one.short_code, "eff")

    def test_templatetag(self):
        one = shorten("http://eff.org")
        template = Template("""{% load short_urls %}{{ "http://eff.org"|shorten }}""")
        self.assertEquals(template.render(Context()), one)

    def test_named_templatetag(self):
        one = ShortURL.objects.shorten("http://eff.org")
        template = Template("""{% load short_urls %}{{ "http://eff.org"|shorten:"eff" }}""")
        self.assertEquals(
            template.render(Context()),
            "http://example.com" + reverse("shorty_expand", args=["eff"])
        )

    def test_short_url_prefix(self):
        with self.settings(SHORT_URL_PREFIX="http://wi.sh/"):
            test = ShortURL.objects.shorten("http://eff.org")
            self.assertEquals(test.format_short_url(),
                              "http://wi.sh/" + test.short_code)

    def test_cache(self):
        cache.clear()

        url = shorten("http://eff.org")
        match = resolve(urlparse(url).path)
        self.assertEquals(
                ShortURL.objects.get(short_code=match.kwargs['short_code']).long_url,
                "http://eff.org")
        # Should hit the cache the second time
        self.assertNumQueries(0, lambda: shorten("http://eff.org"))
        self.assertEquals(
                expand(match.kwargs['short_code']),
                "http://eff.org")
        self.assertNumQueries(0, lambda: expand(match.kwargs['short_code']))
