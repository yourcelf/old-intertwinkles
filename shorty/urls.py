from django.conf.urls.defaults import include, patterns, url

urlpatterns = patterns('shorty.views',
    url(r'(?P<short_code>.*)', 'expand_url', name='shorty_expand'),
)


