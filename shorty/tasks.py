import urlparse

from celery.task import task

from notification.models import Notice

@task
def process_events(url):
    """
    Append any conditional events/trackers from the URL to this (e.g. "notice
    was seen").
    """
    parts = urlparse.urlparse(url)
    if parts.query:
        qargs = urlparse.parse_qs(parts.query)
        if qargs.get("notice_id", None):
            notice = Notice.objects.get(pk=qargs['notice_id'][0])
            notice.unseen = False
            notice.save()
