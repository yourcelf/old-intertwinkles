import random

from django.db import models
from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.conf import settings
from django.core.cache import cache

SHORT_URL_CHARS = getattr(settings, 'SHORT_URL_CHARS', 
    'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
SHORT_URL_LENGTH = getattr(settings, 'SHORT_URL_LENGTH', 4)
SHORT_URL_CACHE_PREFIX = getattr(settings, 'SHORT_URL_CACHE_PREFIX', 'shorty')

def default_short_url_algorithm(long_url):
    while True:
        short_code = "".join(
            random.choice(SHORT_URL_CHARS) for i in range(SHORT_URL_LENGTH)
        )
        if not ShortURL.objects.filter(short_code=short_code).exists():
            return short_code

SHORT_URL_ALGORITHM = getattr(settings, 'SHORT_URL_ALGORITHM',
        default_short_url_algorithm)

class ShortURLManager(models.Manager):
    def shorten(self, value, name=None):
        if value.startswith("http"):
            long_url = value
        else:
            protocol = getattr(settings, "DEFAULT_HTTP_PROTOCOL", "http")
            long_domain = Site.objects.get_current().domain
            long_url = "{0}://{1}{2}".format(
                protocol,
                long_domain,
                value,
            )
        kwargs = {'long_url': long_url}
        if name is not None:
            kwargs['short_code'] = name
        try:
            short = self.filter(**kwargs)[0]
        except IndexError:
            short = ShortURL(**kwargs)
            short.clean()
            short.save()
        return short

class ShortURL(models.Model):
    long_url = models.URLField(max_length=255)
    short_code = models.CharField(max_length=16, unique=True)

    objects = ShortURLManager()

    def clean(self):
        if not self.short_code:
            self.short_code = SHORT_URL_ALGORITHM(self.long_url)

    def format_short_url(self, absolute=True):
        short_prefix = getattr(settings, "SHORT_URL_PREFIX", None)
        if not short_prefix:
            relative = reverse("shorty_expand", args=[self.short_code])
            if absolute:
                protocol = getattr(settings, "DEFAULT_HTTP_PROTOCOL", "http")
                domain = Site.objects.get_current().domain
                short_prefix = "{0}://{1}".format(protocol, domain)
                return "{0}{1}".format(short_prefix, relative)
            return relative
        return short_prefix + self.short_code

def shorten(value, name=None):
    key = SHORT_URL_CACHE_PREFIX + value
    hit = cache.get(key)
    if hit is not None:
        return hit
    short = ShortURL.objects.shorten(value, name)
    short_url = short.format_short_url()
    cache.set(key, short_url)
    cache.set(SHORT_URL_CACHE_PREFIX + short.short_code, short.long_url)
    return short_url

def expand(short_code):
    key = SHORT_URL_CACHE_PREFIX + short_code
    hit = cache.get(key)
    if hit is not None:
        return hit
    try:
        long_url = ShortURL.objects.get(short_code=short_code).long_url
    except ShortURL.DoesNotExist:
        return None
    cache.set(key, long_url)
    return long_url
