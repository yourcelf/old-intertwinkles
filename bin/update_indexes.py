#!/usr/bin/env python

import os
import sys
import datetime
import subprocess

if len(sys.argv) != 2:
    print "Please specify one argument: the seconds before NOW to start indexing"
    sys.exit(1)

try:
    interval = datetime.timedelta(seconds=int(sys.argv[1]))
except ValueError:
    print "Invalid number of seconds: '%s'" % sys.argv[1]
    sys.exit(1)

time = (datetime.datetime.utcnow() - interval).strftime("%Y-%m-%dT%H:%M:%SZ")

cmd = [
    sys.executable,
    os.path.join(os.path.dirname(__file__), "..", "manage.py"),
    "update_index",
    '--start',
    time,
]
sys.exit(subprocess.check_call(cmd))
