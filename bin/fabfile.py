import os
import stat
import time
import json
import shutil
import datetime
import tempfile

from fabric.api import *

BASE = os.path.dirname(os.path.abspath(__file__)).rstrip("/")

env.git_dir = "/var/git/intertwinkles.git"
env.site_dir = "/var/intertwinkles"
env.vepython = "/var/intertwinkles/venv/bin/python"
env.vepip    = "/var/intertwinkles/venv/bin/pip"

def deploy(branch="master"):
    """
    Deploy the local tree to master.  Do your own git committing first.
    """
    with cd(env.site_dir):
        run("git pull origin %s" % branch)
        sudo("su www-data -c '%s manage.py collectstatic --noinput'" % env.vepython)
        sudo("/etc/init.d/apache2 restart")

def deploy_production():
    local("git push production master")
    deploy()

def deploy_testing(branch="master"):
    local("git push testing %s" % branch)
    deploy(branch)

def install_requirements():
    with cd(env.site_dir):
        run("%s install %s/requirements/prod.txt" % (env.vepip, env.site_dir))

def run_migrations():
    with cd(env.site_dir):
        run("%s manage.py syncdb" % env.vepython)
        run("%s manage.py migrate" % env.vepython)

def update_solr_schema():
    with cd(env.site_dir):
        sudo("%s manage.py build_solr_schema > /usr/local/share/apache-solr*/example/solr/conf/schema.xml" % env.vepython)
        sudo("supervisorctl restart intertwinkles-solr")
        run("%s manage.py rebuild_index" % env.vepython)

def rebuild_solr_index():
    with cd(env.site_dir):
        run("%s manage.py rebuild_index" % env.vepython)

def destroy_group(slug):
    with cd(env.site_dir):
        sudo("%s manage.py delete_group %s" % (env.vepython, slug))
        run("%s manage.py rebuild_index" % env.vepython)
