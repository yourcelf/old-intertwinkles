"""
This is a utility file for the django shell.  When you enter the shell,
type:

# >>> from sh import *

and you'll get all the bits you normally want at your fingertips there.
"""
from pprint import pprint
from datetime import datetime, timedelta
from django.utils.timezone import now
from django.core.cache import cache
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import *
from django.contrib.sites.models import *
from django.db import connection

from django_mailman.models import *
from resolve.models import *
from groups.models import *
from embeds.models import *
from profiles.models import *
from timelines.models import *
from shorty.models import *

import groups.tasks
