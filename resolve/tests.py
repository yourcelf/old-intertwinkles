import time
from django.test import TestCase
from django.contrib.auth.models import User
from django.utils.timezone import now
from django.core import mail

from profiles.models import Profile
from groups.models import Group, Membership
from notification.models import get_notification_setting, NoticeType
from resolve.models import Proposal, ProposalRevision, Opinion, Twinkle

class TestNotifications(TestCase):
    def clear_outbox(self):
        for i in range(len(mail.outbox)):
            mail.outbox.pop()

    def setUp(self):
        self.u1 = User.objects.create_user(username="one", email="one@example.com")
        self.u2 = User.objects.create_user(username="two", email="two@example.com")
        self.group = Group.objects.create(name="test", slug="test")
        p = Profile.objects.create(
                user=self.u1,
                mobile_number="5551234567",
                mobile_carrier="T-Mobile",
        )
        p = Profile.objects.create(
                user=self.u2,
                mobile_number="5552345678",
                mobile_carrier="T-Mobile",
        )
        # u1 wants notifications to sms and email; u2 doesn't.
        for user, medium, send in (
                (self.u1, "email", True),
                (self.u1, "sms", True),
                (self.u2, "email", False),
                (self.u2, "sms", False)):
            for label in ("groups_invitation", "resolve_proposal", "resolve_twinkles"):
                notice_setting = get_notification_setting(user,
                        NoticeType.objects.get(label=label),
                        medium)
                notice_setting.send = send
                notice_setting.save()

        for u in (self.u1, self.u2):
            Membership.objects.create(user=u, invited_by=u, group=self.group,
                    invitation_accepted=True, invitation_closed=now())

    def test_proposal_notification(self):
        self.clear_outbox()

        prop = Proposal.objects.create(author=self.u2, group=self.group)
        rev = ProposalRevision.objects.create(
                proposal=prop, author=self.u2, text="Be it resolved...")

        self.assertEquals(
            set(m.to[0] for m in mail.outbox),
            set(['one@example.com', '5551234567@tmomail.net']),
        )

    def test_twinkle_notification(self):
        pass
