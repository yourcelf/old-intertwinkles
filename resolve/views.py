import time
import json
from datetime import datetime, timedelta
from django.shortcuts import get_object_or_404, redirect, render
from django.core.exceptions import PermissionDenied, ObjectDoesNotExist
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST
from django.views.generic import View
from django.contrib import messages
from django.template import loader, Context
from django.http import Http404, HttpResponseBadRequest, HttpResponse
from django.utils.timezone import now
from django.utils.dateparse import parse_datetime

from groups.models import Group
from resolve.models import Proposal, ProposalRevision, Twinkle, Opinion, ProposalVisit
from resolve.forms import ProposalForm, ProposalRevisionForm, OpinionForm
from timelines.models import Event
from intertwinkles.base.utils import json_dt_handler
from profiles.utils import user_public_dict

@login_required
def list_proposals(request, group_id):
    try:
        group = Group.objects.active_for(request.user).get(pk=group_id)
    except Group.DoesNotExist:
        raise Http404

    return render(request, "resolve/list_proposals.html", {
        'current': group.proposal_set.current(),
        'finished': group.proposal_set.finished(),
        'group': group,
        'membership': group.membership_set.current_for(request.user).get(),
    })


@login_required
def edit_proposal(request, proposal_id=None, group_id=None):
    # New proposals pass 'group_id'.  Existing proposals pass 'proposal_id'.
    if (proposal_id is None) and (group_id is None):
        raise Http404

    if proposal_id is None:
        try:
            group = Group.objects.active_for(request.user).get(pk=group_id)
        except Group.DoesNotExist:
            raise PermissionDenied
        proposal = Proposal(group=group, author=request.user)
        revision = ProposalRevision(author=request.user)
    else:
        try:
            proposal = Proposal.objects.active_for(request.user).get(pk=proposal_id)
        except Proposal.DoesNotExist:
            raise Http404
        group = proposal.group
        revision = proposal.current_revision() or ProposalRevision(author=request.user)

    proposal.modified = now()
    proposal_form = ProposalForm(request.POST or None, instance=proposal)
    revision_form = ProposalRevisionForm(request.POST or None, instance=revision)
    if proposal_form.is_valid() and revision_form.is_valid():
        # Skip out if there are no changes.
        if not proposal_form.has_changed() and not revision_form.has_changed():
            return redirect("resolve_show_proposal", proposal.pk)

        proposal = proposal_form.save()
        if revision.author == request.user and revision.created and \
                revision.created > now() - timedelta(seconds=60*3):
            # Within 3 minutes, reuse a previous revision, if any.
            revision = revision_form.save(commit=False)
            revision.proposal = proposal
            revision.save()
            
        else:
            # Otherwise, force new.
            revision = revision_form.save(commit=False)
            revision.pk = None
            revision.proposal = proposal
            revision.created = now()
            revision.author = request.user
            revision.save()
        messages.success(request,
                "Proposal saved.  Thanks for your attention to detail.")
        return redirect("resolve_show_proposal", proposal.pk)
    return render(request, "resolve/edit_proposal.html", {
        'group': group,
        'proposal': proposal,
        'proposal_form': proposal_form,
        'revision_form': revision_form,
    })

@login_required
def show_proposal(request, proposal_id):
    try:
        proposal = Proposal.objects.active_for(request.user).get(pk=proposal_id)
    except Proposal.DoesNotExist:
        raise Http404
    revision = proposal.current_revision()

    try:
        user_opinion = request.user.opinion_set.filter(proposal=proposal).latest()
    except Opinion.DoesNotExist:
        user_opinion = None
    user_membership = None
    current_opinions = {}

    roles = {}  # Current and invited members
    people = {} # Everyone who has had something to do with this proposal, past or present
    for membership in proposal.group.membership_set.current_and_invited():
        people[membership.user.pk] = user_public_dict(membership.user)
        roles[membership.user.pk] = {
            'role': membership.role,
            'voting': membership.voting,
            'start': membership.start,
            'confirmed': membership.invitation_closed is not None,
        }
        if membership.user == request.user:
            user_membership = membership

    # Create the visit event.
    if not proposal.resolved:
        key = Event.objects.get_key(proposal, "visit")
        Event.objects.touch(
                match={
                    'user': request.user,
                    'group': proposal.group,
                    'key': key,
                    'date__gte': now() - timedelta(seconds=60*5),
                },
                update={
                    'date': now(),
                    'user': request.user,
                    'group': proposal.group,
                    'key': key,
                    'data': {
                        'type': 'proposal_visit',
                        'title': 'Visit',
                        'content': loader.get_template(
                                "timelines/events/proposal_visit.html"
                            ).render(Context({'user': request.user})),
                    }
                })


    events = Event.objects.involving_model(proposal)
    timeline = []
    for event in events:
        timeline.append(event.to_dict())
        if event.user_id not in people:
            people[event.user_id] = user_public_dict(event.user)

    twinkles = [t.to_dict() for t in Twinkle.objects.involving_proposal(proposal)]

    opinions = list(Opinion.objects.filter(proposal=proposal, is_current_denormalized=True))
    opinion_data = [o.to_dict() for o in opinions]

    return render(request, "resolve/show_proposal.html", {
        'proposal': proposal,
        'group': proposal.group,
        'revision': proposal.current_revision(),
        'membership': user_membership,
        'opinion': user_opinion,
        'current_opinions': opinions,
        'twinkles': twinkles,
        'opinion_data': json.dumps(opinion_data, default=json_dt_handler, indent=2),
        'timeline_data': json.dumps(timeline, default=json_dt_handler, indent=2),
        'people_data': json.dumps(people, default=json_dt_handler, indent=2),
        'member_data': json.dumps(roles, default=json_dt_handler, indent=2),
    })

@login_required
def edit_opinion(request, proposal_id):
    try:
        proposal = Proposal.objects.select_related().active_for(
                request.user).get(pk=proposal_id)
    except Proposal.DoesNotExist:
        raise Http404
    try:
        opinion = Opinion.objects.filter(proposal_id=proposal_id,
                user=request.user)[0]
    except IndexError:
        opinion = Opinion(user=request.user, proposal=proposal)
    form = OpinionForm(request.POST or None, instance=opinion)
    if form.is_valid():
        opinion = form.save(commit=False)
        # Save new version if it's older than 3 minutes
        if opinion.created and opinion.created < now() + \
                timedelta(seconds=60 * 3):
            opinion.pk = None
            opinion.created = now()
        opinion.save()
        proposal.touch()
        return redirect("resolve_show_proposal", proposal_id)
    return render(request, "resolve/edit_opinion.html", {
        'proposal': proposal,
        'revision': proposal.current_revision(),
        'form': form,
    })

@login_required
@require_POST
def confirm_opinion(request, proposal_id):
    try:
        proposal = Proposal.objects.active_for(request.user).get(pk=proposal_id)
    except Proposal.DoesNotExist:
        raise Http404
    try:
        opinion = Opinion.objects.filter(proposal_id=proposal_id,
                user=request.user)[0]
    except IndexError:
        raise Http404
    opinion.pk = None
    opinion.is_stale_denormalized = False
    opinion.created = now()
    opinion.save()
    proposal.touch()
    return redirect("resolve_show_proposal", proposal_id)

@login_required
@require_POST
def toggle_finished(request, proposal_id):
    try:
        proposal = Proposal.objects.active_for(
                request.user
            ).get(pk=proposal_id)
    except Proposal.DoesNotExist:
        raise Http404
    if proposal.resolved:
        proposal.resolved = None
    else:
        proposal.resolved = now()
    if request.POST.get('approved') == '1':
        proposal.approved = True
    else:
        proposal.approved = False
    proposal.touch() # saves
    return redirect("resolve_show_proposal", proposal_id)

class TwinkleView(View):
    def post(self, request, **kwargs):
        if not request.user.is_authenticated():
            return HttpResponseBadRequest("Login required.")

        kw = json.loads(request.raw_post_data)

        for arg in ("twinkle_type", "object_id"):
            if arg not in kw:
                return HttpResponseBadRequest(
                        "Missing argument {0}".format(arg))

        qs = Proposal.objects.active_for(request.user)

        # Get the model from the twinkle type.
        try:
            Model, attribute_name = Twinkle.model_and_attribute_name_from_twinkle_type(kw['twinkle_type'])
        except KeyError:
            return HttpResponseBadRequest(
                    "Invalid twinkle type: '%s'" % kw['twinkle_type'])
        try:
            obj = Model.objects.active_for(request.user).get(
                    pk=kw['object_id'])
        except ObjectDoesNotExist:
            return HttpResponseBadRequest()

        kwargs = {'user': request.user, attribute_name: obj}
        try:
            twinkle = Twinkle.objects.get(**kwargs)
        except Twinkle.DoesNotExist:
            twinkle = Twinkle(**kwargs)
            twinkle.clean()
            twinkle.save()

        response = HttpResponse(json.dumps(
            twinkle.to_dict(),
            default=json_dt_handler,
        ))
        response['Content-type'] = 'application/json'
        return response 

    def delete(self, request, twinkle_id):
        if not request.user.is_authenticated():
            return HttpResponseBadRequest()
        try:
            twinkle = Twinkle.objects.get(pk=twinkle_id, user=request.user)
        except Twinkle.DoesNotExist:
            raise Http404
        twinkle.delete()
        return HttpResponse()
