{{ object.current_revision.text }}

{% for opinion in object.opinion_set.all %}
{{ opinion.user.profile }}: {{ opinion.get_choice_display }}
{{ opinion.text }}
{% endfor %}
