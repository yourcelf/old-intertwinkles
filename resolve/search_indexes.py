from haystack import indexes
from resolve.models import Proposal

class ProposalIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    title = indexes.CharField(model_attr='get_title')
    group_name = indexes.CharField(model_attr='get_group_name')
    absolute_url = indexes.CharField(model_attr='get_absolute_url')
    created = indexes.DateTimeField(model_attr='created')
    modified = indexes.DateTimeField(model_attr='modified')
    resolved = indexes.DateTimeField(model_attr='resolved', null=True)
    approved = indexes.BooleanField(model_attr='approved')
    group_id = indexes.IntegerField(model_attr='group_id')

    def get_model(self):
        return Proposal

    def get_updated_field(self):
        return 'modified'
