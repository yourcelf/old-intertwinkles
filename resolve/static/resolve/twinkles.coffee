twinkle_template = "
  <img src='/static/img/twinkle_<%= active ? '' : 'in' %>active-32.png'
       alt='Twinkle (<%= active ? '' : 'in' %>active)' />
  <%= count > 0 ? count : '' %>
"

class it.Twinkle extends Backbone.Model
class it.TwinkleCollection extends Backbone.Collection
  model: it.Twinkle
  url: "/resolve/twinkles.json"
  parse: (response) ->
    response.date = new Date(response.date)
    return response

class it.TwinkleView extends Backbone.View
  template: _.template(twinkle_template)
  events:
    'click': 'toggleTwinkle'

  initialize: (options) ->
    @twinkles = options.twinkles
    @people = options.people
    # Attrs to uniquely identify this particular twinkle.
    @unique_attrs = {
      object_id: options.object_id
      twinkle_type: options.twinkle_type
      user_id: options.user_id
    }
    # Attrs to identify all twinkles for this object.
    @type_attrs = {
      twinkle_type: options.twinkle_type
      object_id: options.object_id
    }

  render: =>
    @$el.html(@template({ count: @getCount(), active: @isActive() }))

    # Popover
    unless @secondRender
      @$el.popover {
        title: =>
          if @getCount() > 0
            return "Twinkled by:"
          return "Twinkles:"
        content: =>
          inlines = []
          for model in @twinkles.where(@type_attrs)
            person = @people.get(model.get("user_id"))
            inlines.push(person.get("inline"))
          if inlines.length > 0
            return inlines.join(", ")
          else
            return "No twinkles yet. Click to twinkle."
        placement: "top"
      }
      @secondRender = true

  getCount: => return @twinkles.where(@type_attrs).length
  isActive: => return @twinkles.where(@unique_attrs).length > 0

  toggleTwinkle: =>
    if @isActive()
      to_remove = @twinkles.where(@unique_attrs)
      for obj in to_remove
        obj.destroy()
        @twinkles.trigger("change")
    else
      attrs = _.extend {
        'date': new Date.now()
      }, @unique_attrs
      model = @twinkles.create(attrs)
    @render()
    return false

