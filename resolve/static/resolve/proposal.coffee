opinion_summary_template = "
  <div id='pie'></div>
  <table class='multibar'>
    <% for (var i = 0; i < tallies.length; i++) { %>
      <tr>
        <th><%= tallies[i].label %></th>
        <td class='bar-set choice-<%= tallies[i].choice %>'>
          <% for (var j = 0; j < tallies[i].counts.length; j++) { %>
            <% if (tallies[i].counts[j].count > 0) { %>
              <div class='bar <%= tallies[i].counts[j].className %> popover-trigger'
                   data-title='<%= tallies[i].counts[j].label %>'
                   data-content='<%= tallies[i].counts[j].content %>'
                   data-placement='bottom'
                   data-trigger='hover'
                   style='width: <%= tallies[i].counts[j].percentage %>%' 
                   >
                <%= tallies[i].counts[j].count %>
              </div>
            <% } %>
          <% } %>
        </td>
      </tr>
    <% } %>
  </table>
"

class it.Opinion extends Backbone.Model
class it.OpinionCollection extends Backbone.Collection
  model: it.Opinion


class it.OpinionSummaryView extends Backbone.View
  template: _.template(opinion_summary_template)
  initialize: (options) ->
    @collection = options.collection
    @people = options.people
    @lastRevisedDate = options.lastRevisedDate
    @memberRoles = options.memberRoles

  render: =>
    # Get just the most current opinions from each user...
    current_opinions = {}
    for opinion in @collection.models
        current_opinions[opinion.get("user_id")] = opinion

    # Arrange by choice
    by_choice = {}
    total_count = 0
    for user_id, opinion of current_opinions
      choice = opinion.get("choice")
      unless by_choice[choice]?
        by_choice[choice] = {
          opinions: []
          label: opinion.get("choice_display")
        }
      by_choice[choice].opinions.push(opinion)
      total_count += 1
    
    # Prepare for display.
    tallies = []
    for choice, info of by_choice
      non_voting = []
      stale = []
      current = []
      # Sort opinions by type (current, stale, non-voting)
      for opinion in info.opinions
        total_count += 1
        user = @people.get(opinion.get("user_id"))
        if not @memberRoles[opinion.get("user_id")].voting
          non_voting.push(user)
        else if opinion.get("created") < @lastRevisedDate
          stale.push(user)
        else
          current.push(user)

      counts = []
      for [type, tag] in [[current, ""], [stale, "stale"], [non_voting, "non-voting"]]
        if type.length > 0
          counts.push({
            count: type.length
            content: _.map(type, (u) -> u.get("inline")).join(", ")
            label: info.label + (if tag then " (#{tag})" else "")
            className: tag
            percentage: 100 * (type.length / total_count)
          })

      tallies.push {
        choice: choice
        label: info.label
        counts: counts
      }
    tallies = _.sortBy(tallies, (t) -> -parseInt(t.choice))
      
    missing = []
    for user_id, role of @memberRoles
      if role.voting and not current_opinions[user_id]?
        missing.push(@people.get(user_id).toJSON())

    # Add missing tally.
    if missing.length > 0
      tallies.push {
        choice: "-1"
        label: "No response yet"
        counts: [{
          count: missing.length
          content: _.map(missing, (u) -> u.inline).join(", ")
          label: "Missing"
          className: "missing"
          percentage: 100 * (missing.length / total_count)
        }]
      }

    @$el.html(@template({
      tallies: tallies
    }))
    @$(".popover-trigger").popover()
    return this
