from django.db import models
from django.db.models import F, Max, Q
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.core.exceptions import ValidationError
from django.dispatch import receiver
from django.db.models.signals import post_syncdb, post_save
from django.template import loader, Context
from model_utils.managers import PassThroughManager

from groups.models import Group, GroupQuerySet, PQ, Membership
from timelines.models import Event
from notification import models as notification

class TwinkleQuerySet(models.query.QuerySet):
    def involving_proposal(self, proposal):
        return self.filter(
            Q(proposal=proposal) |
            Q(opinion__proposal=proposal)
        )


class Twinkle(models.Model):
    user = models.ForeignKey(User)
    created = models.DateTimeField(default=now)

    proposal = models.ForeignKey('Proposal', null=True, blank=True)
    opinion  = models.ForeignKey('Opinion',  null=True, blank=True)

    objects = PassThroughManager.for_queryset_class(TwinkleQuerySet)()

    def to_dict(self):
        return {
                'id': self.pk,
                'object_id': self.get_content_object_id(),
                'type': 'Twinkle',
                'twinkle_type': self.twinkle_type(),
                'user_id': self.user_id,
                'date': self.created,
        }

    def get_absolute_url(self):
        return self.get_content_object().get_absolute_url()

    def get_content_object_id(self):
        return self.proposal_id or self.opinion_id

    def get_content_object(self):
        if self.proposal_id: return self.proposal
        if self.opinion_id:  return self.opinion

    def twinkle_type(self):
        if self.proposal_id: return 'Proposal'
        if self.opinion_id:  return 'Opinion'

    @classmethod
    def model_and_attribute_name_from_twinkle_type(self, twinkle_type):
        return {
                'Proposal': (Proposal, "proposal"),
                'Opinion': (Opinion, "opinion"),
        }[twinkle_type]

    def __unicode__(self):
        return " ".join((str(self.user_id), self.created.isoformat()))

    def clean(self):
        possible_props = ("proposal_id", "opinion_id")
        props = []
        for prop in possible_props:
            if getattr(self, prop) is not None:
                props.append(prop)
        if len(props) > 1:
            raise ValidationError("Must have only one of %s" % ", ".join(props))
        elif len(props) == 0:
            raise ValidationError("Must have one of %s" % ", ".join(possible_props))

    class Meta:
        ordering = ['-created']
        get_latest_by = 'created'

class TwinkleNoticeLog(models.Model):
    user = models.ForeignKey(User)
    proposal = models.ForeignKey('Proposal', null=True, blank=True)
    opinion  = models.ForeignKey('Opinion',  null=True, blank=True)
    created = models.DateTimeField(default=now)

class ProposalRevisionNoticeLog(models.Model):
    user = models.ForeignKey(User)
    proposal_revision = models.ForeignKey('ProposalRevision', null=True, blank=True)
    created = models.DateTimeField(default=now)

class ProposalQuerySet(GroupQuerySet):
    class MQ(PQ):
        accessor = "group__membership"

    def current(self):
        return self.filter(resolved__isnull=True)

    def finished(self):
        return self.filter(resolved__isnull=False)

    def response_needed_from(self, user):
        return self.current().active_for(user).annotate(
                latest_proposal=Max('proposalrevision__created'),
            ).exclude(
                Q(opinion__user=user) & Q(opinion__created__gte=now())
            )

    def needing_attention_from(self, user):
        return self.active_for(user).current().exclude(
               opinion__is_stale_denormalized=False,
               opinion__user=user
           )

    def recently_attended_by(self, user):
        return self.active_for(user).current().filter(
                opinion__is_stale_denormalized=False,
                opinion__user=user,
        )

class Proposal(models.Model):
    author   = models.ForeignKey(User)
    group    = models.ForeignKey(Group)
    created  = models.DateTimeField(default=now)
    modified = models.DateTimeField(default=now)
    deadline = models.DateTimeField(blank=True, null=True)
    resolved = models.DateTimeField(blank=True, null=True)
    approved = models.BooleanField()

    objects = PassThroughManager.for_queryset_class(ProposalQuerySet)()

    def touch(self):
        self.modified = now()
        self.save()

    def get_title(self):
        return self.current_revision().get_title()

    def get_group_name(self):
        return self.group.name

    def get_absolute_url(self):
        return reverse("resolve_show_proposal", kwargs={'proposal_id': self.pk})

    def current_revision(self):
        try:
            return self.proposalrevision_set.all()[0]
        except IndexError:
            return None

    def previous_revision(self):
        try:
            return self.proposalrevision_set.all()[1]
        except IndexError:
            return None

    
    def __unicode__(self):
        return self.created.isoformat()

    class Meta:
        ordering = ['-created']
        get_latest_by = 'created'

class ProposalRevision(models.Model):
    proposal = models.ForeignKey(Proposal)
    author = models.ForeignKey(User)
    text = models.TextField()
    created = models.DateTimeField(default=now)

    def get_absolute_url(self):
        return self.proposal.get_absolute_url()

    def get_title(self):
        if len(self.text) < 50:
            return self.text
        return self.text[0:47] + "..."

    def is_first_revision(self):
        return self.proposal.proposalrevision_set.count() == 1

    def __unicode__(self):
        return " ".join((str(self.author_id), self.created.isoformat()))

    class Meta:
        ordering = ['-created']
        get_latest_by = 'created'


class ProposalVisitQuerySet(models.query.QuerySet):
    def latest_for_user(self, user):
        try:
            return self.filter(user=user)[0]
        except IndexError:
            return None

class ProposalVisit(models.Model):
    user = models.ForeignKey(User)
    proposal = models.ForeignKey(Proposal)
    date = models.DateTimeField(default=now)

    objects = PassThroughManager.for_queryset_class(ProposalVisitQuerySet)()

    class Meta:
        ordering = ['-date']
        get_latest_by = 'date'

OPINION_CHOICES = (
    ('5', _('Strongly approve')),
    ('4', _('Approve with reservations')),
    ('3', _('Need more discussion')),
    ('2', _('Have concerns')),
    ('1', _('Block')),
    ('0', _('I have a conflict of interest.')),
)

class OpinionQuerySet(GroupQuerySet):
    class MQ(PQ):
        accessor = "proposal__group__membership"

class Opinion(models.Model):
    user = models.ForeignKey(User)
    proposal = models.ForeignKey(Proposal)
    created = models.DateTimeField(default=now)
    choice = models.CharField(max_length=1, choices=OPINION_CHOICES)
    text = models.TextField(blank=True,
            verbose_name="Details",
            help_text="Please explain your position."
        )
    # "stale" means the opinion was made before the proposal was revised.
    is_stale_denormalized = models.BooleanField(default=False)
    # "current" means the latest opinion by a particular user.
    is_current_denormalized = models.BooleanField(default=True)

    objects = PassThroughManager.for_queryset_class(OpinionQuerySet)()

    def to_dict(self):
        return {
            'id': self.pk,
            'user_id': self.user_id,
            'proposal_id': self.proposal_id,
            'created': self.created,
            'choice': self.choice,
            'choice_display': self.get_choice_display(),
            'text': self.text,
        }

    def current_revision(self):
        try:
            return self.opinionrevision_set.all()[0]
        except IndexError:
            return None

    def get_absolute_url(self):
        return self.proposal.get_absolute_url()

    def get_membership(self):
        return Membership.objects.active(when=self.created).filter(
                user=self.user, 
                group=self.proposal.group)[0]

    def __unicode__(self):
        return " ".join((str(self.user_id), str(self.proposal_id), self.created.isoformat()))

    class Meta:
        ordering = ['-created']
        get_latest_by = 'created'


#
# Notification and Event signals
#

@receiver(post_syncdb, sender=notification)
def create_notice_types(app, created_models, verbosity, **kwargs):
    notification.create_notice_type("resolve_twinkles",
            _("Twinkles"),
            _("Someone twinkled your post."))
    notification.create_notice_type("resolve_proposal",
            _("Proposal changed"),
            _("Someone revised a proposal in one of your groups."))

@receiver(post_save, sender=Proposal)
def send_response_notification(sender, instance=None, **kwargs):
    if instance is None or 'created' not in kwargs:
        return
    proposal = instance

@receiver(post_save, sender=ProposalRevision)
def send_revision_notification(sender, instance=None, **kwargs):
    if instance is None or 'created' not in kwargs:
        return

    # Create event
    revision = instance
    t = loader.get_template("timelines/events/proposal_revision.html")
    Event.objects.create(
        user=revision.author,
        group=revision.proposal.group,
        key=Event.objects.get_key(revision.proposal, "proposal_revision"),
        data={
            'type': 'proposal_revision',
            'title': "Proposal Revised",
            'content': t.render(Context({'revision': revision}))
        })

    # Create notification
    recipients = []
    for member in revision.proposal.group.membership_set.active().exclude(
            user_id=revision.author_id):
        log, created = ProposalRevisionNoticeLog.objects.get_or_create(
                user=member.user, proposal_revision=revision)
        if created:
            recipients.append(member.user)
    if recipients:
        notification.send(
                users=recipients,
                label="resolve_proposal",
                extra_context={'revision': revision},
                sender=revision.author,
                on_site=False)

@receiver(post_save, sender=Twinkle)
def send_twinkle_notifications(sender, instance=None, **kwargs):
    if instance is None or 'created' not in kwargs:
        return
    twinkle = instance
    obj = twinkle.get_content_object()
    if twinkle.twinkle_type() == 'Proposal':
        proposal = obj
        author = proposal.author
    else:
        proposal = obj.proposal
        author = obj.user

    # Create event
    t = loader.get_template("timelines/events/twinkle.html")
    Event.objects.create(
            user=author,
            group=proposal.group,
            key=Event.objects.get_key(proposal, 'twinkle'),
            data={
                'type': 'twinkle',
                'title': "New twinkle",
                'content': t.render(Context({
                    'twinkle': twinkle,
                    'object': obj,
                    'proposal': proposal,
                    'author': author,
                }))
            })

    # Create notification
    if author != twinkle.user:
        tnl, created = TwinkleNoticeLog.objects.get_or_create(
            user=twinkle.user,
            proposal=twinkle.proposal,
            opinion=twinkle.opinion
        )
        if created:
            notification.send(
                    [author],
                    "resolve_twinkles",
                    {'object': obj},
                    sender=twinkle.user,
                    on_site=True,
            )

@receiver(post_save, sender=Opinion)
def create_opinion_event(sender, instance=None, **kwargs):
    if instance is None or 'created' not in kwargs:
        return
    opinion = instance
    t = loader.get_template("timelines/events/opinion.html")
    Event.objects.create(
            user=opinion.user,
            group=opinion.proposal.group,
            key=Event.objects.get_key(opinion.proposal, 'opinion'),
            data={
                'type': 'opinion',
                'title': "New response",
                'content': t.render(Context({
                    'opinion': opinion,
                }))
            })

#
# Denormalization signals
#

@receiver(post_save, sender=ProposalRevision)
def denormalize_opinion_staleness(sender, instance=None, **kwargs):
    """
    Set the denormalized 'is_stale_denormalized' attribute on all Opinions for
    this proposal.  This dramatically simplifies queries searching for the list
    of all proposals that are stale for particular users.
    """
    if instance is not None:
        revision = instance
        Opinion.objects.filter(proposal_id=revision.proposal_id,
                created__lte=revision.created).update(is_stale_denormalized=True)
        Opinion.objects.filter(proposal_id=revision.proposal_id,
                created__gt=revision.created).update(is_stale_denormalized=False)

@receiver(post_save, sender=Opinion)
def denormalize_opinion_currency(sender, instance=None, **kwargs):
    """
    Set the denormalized 'is_current_denormalized' attribute on all Opinions
    for this user and proposal.  This dramatically simplifies queries searching
    for the list of all opinions that are current for a proposal.
    """
    if instance is not None:
        opinion = instance
        Opinion.objects.filter(
                proposal_id=opinion.proposal_id,
                user_id=opinion.user_id,
                created__lt=opinion.created
            ).update(is_current_denormalized=False)

