from django.conf.urls.defaults import include, patterns, url

from resolve.views import TwinkleView

urlpatterns = patterns('resolve.views',
    url(r'^proposal/group/(?P<group_id>\d+)', 'list_proposals',
        name='resolve_list_proposals'),
    url(r'^proposal/new/(?P<group_id>\d+)', 'edit_proposal',
        name='resolve_add_proposal'),
    url(r'^proposal/(?:(?P<proposal_id>\d+)/)?edit$', 'edit_proposal',
        name='resolve_edit_proposal'),
    url(r'^proposal/(?P<proposal_id>\d+)$', 'show_proposal',
        name='resolve_show_proposal'),
    url(r'^proposal/(?P<proposal_id>\d+)/opinion/edit$', 'edit_opinion',
        name='resolve_edit_opinion'),
    url(r'^proposal/(?P<proposal_id>\d+)/opinion/confirm$', 'confirm_opinion',
        name='resolve_confirm_opinion'),
    url(r'^proposal/(?P<proposal_id>\d+)/toggle_finished$', 'toggle_finished',
        name='resolve_proposal_toggle_finished'),
    url(r'^twinkles.json(?:/(?P<twinkle_id>\d+))?$', TwinkleView.as_view()),
)

