from django import forms
from django.core.exceptions import ValidationError

from resolve.models import Proposal, ProposalRevision, Opinion
from intertwinkles.base.forms import DateJSWidget

class ProposalForm(forms.ModelForm):
    deadline = forms.DateTimeField(widget=DateJSWidget, required=False)

    class Meta:
        model = Proposal
        fields = ['deadline']

class ProposalRevisionForm(forms.ModelForm):
    class Meta:
        model = ProposalRevision
        fields = ['text']

class OpinionForm(forms.ModelForm):
    class Meta:
        model = Opinion
        fields = ['choice', 'text']
