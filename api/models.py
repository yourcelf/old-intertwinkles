#from django.db import models
#from django.core.exceptions import ValidationError
#
#from django.contrib.auth.models import User
#from groups.models import Group
#
## Create your models here.
#class Share(models.Model):
#    key = models.CharField(max_length=255)
#    user_url = models.URLField()
#    index_url = models.URLField()
#    owner = models.ForeignKey(User)
#    public_view = models.BooleanField(
#            help_text="If true, anyone can look at this share."
#    )
#    public_edit = models.BooleanField(
#            help_text="If true, anyone can edit this share. Implies anyone can view."
#    )
#    public_index = models.BooleanField(
#            help_text="If true, in addition to being viewable, the share is publicly listed."
#    )
#    groups = models.ManyToManyField(Group, null=True, blank=True)
#    users  = models.ManyToManyField(User, null=True, blank=True)
#
#    def clean(self):
#        if not self.public_view and (self.public_index or self.public_edit):
#            raise ValidationError("Inconsistent permissions: public_view must be True if public_edit or public_index are.")
