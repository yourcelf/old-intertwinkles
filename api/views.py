import json
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseNotFound, HttpResponseBadRequest
from django.views.generic import View
from django.utils.decorators import method_decorator
from django.conf import settings
from django.shortcuts import render

from django.contrib.auth.models import User
from groups.models import Group
from timelines.models import Event
import profiles.utils
from profiles.models import Profile, Icon

dthandler = lambda obj: obj.isoformat() if hasattr(obj, "isoformat") else None

class JSONView(View):
    http_method_names = ['get', 'options']

    def json_response(self, struct, response=None):
        response = response or HttpResponse()
        response.content = json.dumps(struct, indent=1, default=dthandler)
        response['Content-type'] = "application/json"
        return response

    def check_request(self, request, required, allowed, method):
        bad_api_key = self.check_api_key(request)
        if bad_api_key: return bad_api_key

        missing = self.required_params(request, required, method=method)
        if missing: return missing

        unhandled = self.allowed_params(request, allowed, method=method)
        if unhandled: return unhandled

        return None

    def forbidden_response(self):
        return self.json_response({'error': 'Unauthorized'},
                HttpResponseForbidden())

    def not_found_response(self):
        return self.json_response({'error': 'Not found'},
                HttpResponseNotFound())

    def absolutize_url(self, url):
        return settings.SITE_URL.rstrip("/") + url

    def required_params(self, request, params, method='GET'):
        querydict = getattr(request, method)
        missing = []
        for param in params:
            if param not in querydict or querydict[param] == "":
                missing.append(param)
        if missing:
            return self.json_response({
                'error': "Missing required params: %s" % missing
            }, HttpResponseBadRequest())
        return None

    def allowed_params(self, request, params, method='GET'):
        querydict = getattr(request, method)
        unhandled = []
        for key in querydict:
            if key not in params:
                unhandled.append(key)
        if unhandled:
            return self.json_response({
                'error': "Unknown params: %s" % unhandled
            }, HttpResponseBadRequest())
        return None

    def get_user(self, request, method='GET'):
        email = getattr(request, method).get('user')
        return User.objects.get(email=email)

    def serialize_user(self, user):
        u = {
            'id': user.id,
            'email': user.email,
            'name': user.email,
            'icon': {
                'color': '000000',
            }
        }
        p = user.profile
        if p:
            u.update({
                'name': p.name,
                'icon': {
                    'id': p.source_icon_id,
                    'color': p.icon_color,
                    'name': p.source_icon.name,
                    'tiny': self.absolutize_url(
                        p.get_icon_url("16")),
                    'small': self.absolutize_url(
                        p.get_icon_url("32")),
                    'medium': self.absolutize_url(
                        p.get_icon_url("64")),
                    'large': self.absolutize_url(
                        p.get_icon_url("128")),
                },
            })
        return u

    def check_api_key(self, request):
        api_key = request.GET.get('api_key') or request.POST.get('api_key')
        error = {}
        if api_key is None:
            error['error'] = "Missing required `api_key` param."
        elif api_key not in settings.AUTHORIZED_API_KEYS:
            error['error'] = "Invalid api key."
        if error:
            return self.json_response(error, HttpResponseForbidden())
        return None

class Groups(JSONView):
    def get(self, request):
        bad = self.check_request(request, 
            required=["user", "api_key"],
            allowed=["user", "api_key", "group_id", "slug"],
            method='GET')
        if bad: return bad
        
        struct = {
            'groups': [],
            'messages': [],
            'users': {},
        }

        # Get or create user account.
        try:
            user = self.get_user(request)
        except User.DoesNotExist:
            user = None
        finally:
            if user == None:
                user = profiles.utils.get_or_create_user(
                        email=request.GET.get("user")) 
                user.profile = Profile(
                        user=user, name="Person %s" % user.pk)
                user.profile.full_clean()
                user.profile.save()
                struct['messages'].append("NEW_ACCOUNT")

        struct['users'][user.pk] = self.serialize_user(user)

        groups = Group.objects.current_for(user)
        if request.GET.get("group_id"):
            groups = groups.filter(pk=request.GET['group_id'])
        if request.GET.get("slug"):
            groups = groups.filter(slug=request.GET['slug'])

        for g in groups:
            group = {
                'id': g.id,
                'name': g.name,
                'slug': g.slug,
                'logo': {
                },
                'members': [],
                'absolute_url': self.absolutize_url(g.get_absolute_url()),
                'edit_url': self.absolutize_url(g.get_edit_url()),
            }
            if g.logo:
                group['logo'].update({
                    'url': g.logo.url,
                    'height': g.logo.height,
                    'width': g.logo.width,
                })
            struct['groups'].append(group)
            for m in g.membership_set.current_and_invited().select_related(
                    'user', 'user__profile', 'user__profile__source_icon'):
                u = m.user
                membership = {
                    'id': m.id,
                    'start': m.start,
                    'end': m.end,
                    'invited_by': m.invited_by_id,
                    'invitation_closed': m.invitation_closed,
                    'invitation_accepted': m.invitation_accepted,
                    'subscribe_to_mailing_list': m.subscribe_to_mailing_list,
                    'role': m.role,
                    'user_id': u.id,
                }
                group['members'].append(membership)

                if u.id not in struct['users']:
                    struct['users'][u.id] = self.serialize_user(u)
        return self.json_response(struct)

class Events(JSONView):
    http_method_names = ['get', 'put', 'delete', 'options']

    def get(self, request):
        bad = self.check_request(request,
                required=["user", "api_key"],
                allowed=["user", "api_key", "key", "group_id", "user_id"])
        if bad: return bad

        try:
            user = self.get_user(request)
        except User.DoesNotExist:
            return self.not_found_response()

        key = request.GET.get('key')
        events = Event.objects.current_for(user).filter(key__startswith=key)
        if request.GET.get('group_id'):
            events = events.filter(group_id=request.GET['group_id'])
        if request.GET.get('user_id'):
            events = events.filter(user_id=request.GET['user_id'])

        events = events.select_related(
                'user', 'user__profile', 'user__profile__source_icon'
            )[:50]
        struct = {
            'events': [],
            'users': {},
        }
        for event in events:
            struct['events'].append({
                'key': event.key,
                'date': event.date,
                'user_id': event.user_id,
                'group_id': event.group_id,
                'data': event.data,
            })
            if event.user_id not in struct['events']:
                struct['users'][event.user_id] = self.serialize_user(event.user)
        return self.json_response(struct)

class Profiles(JSONView):
    http_method_names = ['post']

    def post(self, request):
        params = ["api_key", "user", "name", "icon_id", "icon_color"]
        bad = self.check_request(request, required=params,
                allowed=params, method='POST')
        if bad: return bad

        try:
            user = self.get_user(request, method='POST')
        except User.DoesNotExist:
            return self.not_found_response()

        profile = user.profile
        profile.name = request.POST.get("name")
        try:
            profile.source_icon = Icon.objects.get(pk=request.POST.get("icon_id"))
        except Icon.DoesNotExist:
            return self.not_found_response()
        profile.icon_color = request.POST.get("icon_color")
        profile.clean()
        profile.save()
        return self.json_response(self.serialize_user(user))

class Notifications(JSONView):
    http_method_names = ['post', 'get']
    def get(self, request):
        params = ["api_key", "user"]
        bad = self.check_request(request, params, params, "GET")
        if bad: return bad

        try:
            user = self.get_user(request, method='GET')
        except User.DoesNotExist:
            return self.not_found_response()

       # List the notifications available for the user. 

    def post(self, request):
        pass


def auth_frame(request):
    return render(request, "api/auth_frame.html")
