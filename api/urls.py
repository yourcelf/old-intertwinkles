from django.conf.urls.defaults import include, patterns, url
from django.views.decorators.csrf import csrf_exempt

import views

urlpatterns = patterns('',
    url(r'^groups/$', views.Groups.as_view()),
    url(r'^events/$', views.Events.as_view()),
    url(r'^profiles/$', csrf_exempt(views.Profiles.as_view())),
    url(r'auth_frame/$', views.auth_frame),
)
