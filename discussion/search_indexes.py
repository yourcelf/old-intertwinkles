from haystack import indexes
from django.core.urlresolvers import reverse
from django_mailman.models import ListMessage

class ListMessageIndex(indexes.SearchIndex, indexes.Indexable):
    text  = indexes.CharField(document=True, use_template=True)
    title = indexes.CharField(model_attr='subject')
    date = indexes.DateTimeField(model_attr='date')
    type  = indexes.CharField()
    group_name = indexes.CharField()
    group_id = indexes.IntegerField()
    absolute_url = indexes.CharField()

    def prepare_type(self, msg):
        return "message"

    def prepare_group_name(self, msg):
        self.prepare_group_id(msg)
        return msg._group.name

    def prepare_group_id(self, msg):
        if not hasattr(msg, "_group"):
            msg._group = msg.list.group_set.get()
        return msg._group.pk

    def prepare_absolute_url(self, msg):
        return reverse("discussion:show_thread", args=[
                    self.prepare_group_id(msg),
                    msg.pk
                ])

    def get_model(self):
        return ListMessage

    def get_updated_field(self):
        return 'date'
