from django.conf.urls.defaults import include, patterns, url

group_id = r'(?P<group_id>\d+)'
message_id = r'(?P<message_id>\d+)'

urlpatterns = patterns('discussion.views',
    url(r'^{0}/{1}$'.format(group_id, message_id), 'show_thread', name='show_thread'),
    url(r'^{0}/$'.format(group_id), 'list_threads', name='list_threads'),
)
