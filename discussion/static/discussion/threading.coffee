# equivalent to python's string.endswith(substr)
endswith = (a, b) -> return a.substr(a.length - b.length, b.length) == b

# equivalent to python's string.startswith(substr)
startswith = (a, b) -> return a.substr(0, b.length) == b

# equivalent to python's string.count(substr)
count_occurrances = (str, substr) ->
  n = 0
  pos = 0
  while true
    pos = str.indexOf(substr, pos)
    if pos >= 0
      n += 1
      pos += substr.length
    else
      return n

# rsplit(str, separator, max) equivalent to python's str.rsplit(sep, max)
rsplit = (str, sep, max) ->
  split = str.split(sep || /\s+/)
  if max?
    return [ split.slice(0, -max).join(sep) ].concat(split.slice(-max))
  return split

# Adaptation of django.utils.html.urlize
simple_url_re = /^https?:\/\/\w/
simple_url_2_re = /^www\.|^(?!http)\w[^@]+\.(com|edu|gov|int|mil|net|org)$/
simple_email_re = /^\S+@\S+\.\S+$/
urlize = (text, trim_url_limit=null, nofollow=false) ->
  trim_url = (x) ->
    if trim_url_limit? and x.length > trim_url_limit
      return "#{x.substr(0, trim_url_limit - 3)}..."
    return x

  words = text.split(/(\s+)/)
  for word, i in words
    if /[\.@:]/.test(word)
      [lead, middle, trail] = ['', word, '']
      for punctuation in ['.', ',', ':', ';']
        if endswith(middle, punctuation)
          middle = middle.substr(0, middle.length - punctuation.length)
          trail = punctuation + trail
      for opening, closing in [['(', ')'], ['<', '>'], ['&lt;', '&gt;']]
        if startswith(middle, opening)
          middle = middle.substr(opening.length, middle.length - opening.length)
          lead = lead + opening
        if endswith(middle, closing) and (count_occurrances(middle, opening) == count_occurrances(middle, closing))
          middle = middle.substr(0, middle.length - closing.length)
          trail = closing + trail
      # Build URL to point to.
      url = null
      nofollow_attr = if nofollow then ' rel="nofollow"' else ''
      if simple_url_re.test(middle)
        url = middle
      else if simple_url_2_re.test(middle)
        url = "http://#{middle}"
      else if (not /:/.test(middle)) and simple_email_re.test(middle)
        [local, domain] = rsplit(middle, '@', 1)
        url = 'mailto:#{local}@#{domain}'

      if url
        trimmed = trim_url(middle)
        words[i] = "#{lead}<a href=\"#{url}\"#{nofollow_attr}>#{trimmed}</a>#{trail}"
  return words.join("")

# Mark up the html all pretty-like &c.
process_body = ->
  $(".body").each ->
    contents = $(this).html()
    contents = urlize(contents, trim_url_limit=null, nofollow=true)
    contents = contents.replace(/^(On [^\n]+wrote:)(?=[\n\n\s]*&gt;)/gm, "<span class='quote'>$1</span>")
    contents = contents.replace(/^(&gt;.*)$/mg, "<span class='quote'>$1</span>")
    assemble = []
    quote_block = []
    quote_block_count = 0
    for line in contents.split("\n")
      if startswith(line, "<span class='quote'>")
        quote_block_count += 1
        quote_block.push(line)
        continue
      else if quote_block_count > 5
        assemble.push("<div class='quote-block'>#{quote_block.join("\n")}</div>")
      else if quote_block_count > 0
        assemble = assemble.concat(quote_block)
      assemble.push(line)
      quote_block = []
      quote_block_count = 0
    $(this).html(assemble.join("\n"))

  $("div.quote-block").each (i) ->
    block = $(this)
    toggle = $("<a class='quote-toggle' href='#'>Show quoted text</a>")
    toggle.on "click", ->
      block.toggle()
      if block.is(":visible")
        toggle.html("Hide quoted text")
      else
        toggle.html("Show quoted text")
      return false
    toggle.insertBefore(block)
    block.hide()

  $(".thread-nav").on "click", ->
    if window.history and window.history.pushState
      href = $(this).attr("href")
      $.get href, (data) ->
        $("#discussion_loading").hide()
        new_doc = $("<div>" + data + "</div>")
        $(".discussion").html($(".discussion", new_doc).html())
        $(".discussion").show("slide", {
          direction: if rtl then "right" else "left"
        })
        $("title").html($("title", new_doc).html())
        process_body()

      rtl = $(this).hasClass("right")
      history.pushState({}, "", href)
      $(".discussion").hide("slide", {
        direction: if rtl then "left" else "right"
      }, -> $("#discussion_loading").show())
      return false
process_body()
