from django import template
from django.template.defaultfilters import stringfilter
from shorty.models import shorten as shorty_shorten
from shorty.models import expand as shorty_expand

register = template.Library()

@register.filter(name='smart_page_range')
def smart_page_range(page):
    """
    Show the whole page range in 20 entries.  Show the two pages before and after the current, and 15 more pages that cover the whole range.  The last page is always the last page, and first page is always the first page.  Example:

    >>> smart_filter_range(Paginator(range(1, 5)).page(2))
    [1, 2, 3, 4, 5]
    >>> smart_filter_range(Paginator(range(1, 21)).page(2))
    [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
    >>> smart_filter_range(Paginator(range(1, 100)).page(50))
    [1, 10, 20, 30, 40, 48, 49, 50, 51, 52, 60, 70, 80, 90, 99]
    >>> smart_filter_range(Paginator(range(1, 1000)).page(1))
    [1, 2, 3, 100, 200, 300, 400, 500, 600, 700, 800, 900, 999]

    """
    count = page.paginator.num_pages
    if count < 15:
        return page.paginator.page_range
    # Add the limits of the paginator.
    nums = [1, count]
    # Add two before, and two after the current:
    nums += range(
            max(1, page.number - 2),
            min(count, page.number + 3))
    # Add nearest multiple of 5 or 10 that covers the range in under 12 steps.
    step = max(1, 5 * (int(int(count / 12) / 5)))
    nums += range(step, count, step)

    # Uniquify and sort the list.
    nums = list(set(nums))
    nums.sort()
    print nums
    return nums
