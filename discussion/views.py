from django.shortcuts import get_object_or_404, render 
from django.http import Http404
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django_mailman.models import ListMessage, List
from groups.models import Group

@login_required
def show_thread(request, group_id, message_id):
    try:
        group = Group.objects.active_for(request.user, pk=group_id).get()
    except Group.DoesNotExist:
        raise Http404
    message = get_object_or_404(ListMessage,
            list=group.mailing_list,
            pk=message_id)

    urparent = message.urparent_denormalized
    thread = ListMessage.objects.filter(urparent_denormalized=urparent)

    try:
        next_msgs = list(ListMessage.objects.filter(
                list__group=group,
                thread_depth_denormalized=0,
                date__gt=urparent.date).order_by('date')[0:10])
    except IndexError:
        next_msgs = None

    try:
        prev_msgs = list(ListMessage.objects.filter(
                list__group=group,
                thread_depth_denormalized=0,
                date__lt=urparent.date).order_by('-date')[0:10])
    except IndexError:
        prev_msgs = None

    return render(request, "discussion/show_thread.html", {
        'group': group,
        'membership': group.membership_set.current_for(request.user).get(),
        'message': message,
        'thread': thread,
        'next_msgs': next_msgs,
        'prev_msgs': prev_msgs,
        'latest_msgs': ListMessage.objects.filter(
            list__group=group,
            thread_depth_denormalized=0).order_by('-date')[0:10]
            
    })

@login_required
def list_threads(request, group_id):
    try:
        group = Group.objects.active_for(request.user, pk=group_id).get()
    except Group.DoesNotExist:
        raise Http404
    
    messages = ListMessage.objects.filter(list__group=group).order_by('-date')

    paginator = Paginator(messages, 50)
    try:
        page = paginator.page(request.GET.get('page'))
    except PageNotAnInteger:
        page = paginator.page(1)
    except EmptyPage:
        raise Http404

    return render(request, "discussion/list_threads.html", {
        'group': group,
        'membership': group.membership_set.current_for(request.user).get(),
        'page': page,
    })

