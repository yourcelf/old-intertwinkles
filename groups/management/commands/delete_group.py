import os
from sys import exit
import random
import subprocess
from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ObjectDoesNotExist

from groups.models import Group

class Command(BaseCommand):
    args = '<group_slug group_slug ...>'
    help = 'Non-reversibly destroy the specified group and local mailman list.'

    def handle(self, *args, **options):
        groups = []
        if os.geteuid() != 0:
            exit("Please run this command as root.")

        for slug in args:
            try:
                groups.append(Group.objects.get(slug=slug))
            except Group.DoesNotExist:
                raise CommandError('Group "%s" does not exist' % slug)

        print("You've asked to delete {0} group(s)".format(len(groups)))
        for group in groups:
            print "============================"
            print group.name, "(%s)" % group.slug
            print"Members:", ", ".join(
                m.user.email for m in group.membership_set.all()
            )
            print "Proposals:", group.proposal_set.count()
            print "Embeds:", group.embed_set.count()
            try:
                print "Emails:", group.mailing_list.listmessage_set.count()
            except (ObjectDoesNotExist, AttributeError):
                pass
            print "============================"
            if os.path.exists("/var/lib/mailman/lists/{0}".format(group.slug)):
                print("Local mailng List: "
                      "/var/lib/mailman/lists/{0}".format(group.slug))

        # Confirm
        with open("/usr/share/dict/words") as fh:
            words = fh.read().split("\n")
        randy = " ".join(random.choice(words) for i in xrange(3))
        print
        print("This can't be undone.")
        print("To ensure you're serious, "
              "please type the following phrase exactly:")
        print(randy)
        if raw_input("> ") != randy:
            exit("Codes didn't match; aborting.")

        # Destroy
        for group in groups:
            if os.path.exists(
                    "/var/lib/mailman/lists/{0}".format(group.slug)):
                subprocess.check_call(["/usr/sbin/rmlist",
                    "-a", group.mailing_list.name])
            try:
                group.mailing_list.delete()
            except (ObjectDoesNotExist, AttributeError):
                pass
            group.delete()
