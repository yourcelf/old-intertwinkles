from django.core.management.base import BaseCommand
from django.utils.timezone import now

import requests

from groups.models import Group
from django_mailman.models import List, ListMessage
from django_mailman.management.commands.denormalize_threads import Command as Denormalize

class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        for group in Group.objects.filter(enable_mailing_list=True):
            # Assume that the list is private.
            try:
                ml = group.mailing_list
            except List.DoesNotExist:
                continue
            results = ListMessage.objects.create_from_archive(ml, now())
            if results is None:
                print "No archive found for %s mailing list." % group.name
            else:
                print "Fetched %s new messages for %s" % (len(results), group.name)
        Denormalize().handle()
