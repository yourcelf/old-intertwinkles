from django.core.management.base import BaseCommand

from groups.models import Group
from groups import tasks

class Command(BaseCommand):
    args = ''
    help = 'Update the settings for all mailing lists using web calls.'

    def handle(self, *args, **kwargs):
        for group in Group.objects.all():
            tasks.update_mailing_list_subscriptions(group_id=group.pk)
            tasks.update_mailing_list_settings(group_id=group.pk)


