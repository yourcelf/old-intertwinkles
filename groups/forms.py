from django import forms
from django.forms.formsets import formset_factory
from haystack.forms import SearchForm
from haystack.inputs import Raw

from groups.models import Group, Membership

class GroupForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(GroupForm, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            self.fields['slug'].widget.attrs['readonly'] = True
            self.fields['slug'].help_text = "This is the name of your mailing list - it can't be changed."
        else:
            # Unsaved groups can't be marked active.  Simplify!
            self.fields.pop('active')

    class Meta:
        model = Group
        exclude = ['mailing_list', 'logo_width', 'logo_height']

class GroupSearchForm(SearchForm):
    def __init__(self, group_ids, *args, **kwargs):
        """
        A search constrained to the specified groups.
        """
        super(GroupSearchForm, self).__init__(*args, **kwargs)
        self.group_ids = group_ids

    def search(self):
        sqs = super(GroupSearchForm, self).search()
        sqs = sqs.filter(group_id=Raw((" OR ".join(str(g) for g in self.group_ids))))
        return sqs
