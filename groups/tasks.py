import sys
from email.utils import parseaddr
from celery.task import task
from groups.models import Group
from django_mailman.models import List
from django.conf import settings

if not hasattr(settings, "MAILMAN"):
    sys.stderr.write("Missing MAILMAN settings.\n")
else:
    MAILMAN = settings.MAILMAN

@task
def update_mailing_list_subscriptions(group_id):
    group = Group.objects.get(pk=group_id)
    if group.enable_mailing_list: 
        try:
            mailing_list = List.objects.get(name=group.slug)
        except List.DoesNotExist:
            mailing_list = List.create_list(
                    main_url=MAILMAN['main_url'],
                    email_domain=MAILMAN['email_domain'],
                    admin_pass=MAILMAN['admin_password'],
                    owner_email=MAILMAN['owner_email'],
                    owner_password=MAILMAN['owner_password'],
                    language=MAILMAN.get('language', 'en'),
                    listname=group.slug,
                    list_encoding=MAILMAN.get('list_encoding', 'iso-8859-1'),
            )
            mailing_list.save()
        group.mailing_list = mailing_list
        group.save()
        update_mailing_list_settings(group_id)

        cur_emails = set(
                parseaddr(m)[1] for m in group.mailing_list.get_all_members()
        )
        desired_emails = set(
                m.user.email for m in group.membership_set.active().on_mailing_list()
        )

        should_subscribe   = desired_emails - cur_emails
        should_unsubscribe = cur_emails - desired_emails

        for email in should_subscribe:
            group.mailing_list.subscribe(email)
        for email in should_unsubscribe:
            group.mailing_list.unsubscribe(email)
    else: 
        if group.mailing_list:
            for member in group.members.active():
                group.mailing_list.unsubscribe(member.user.email)
            group.mailing_list = None
            group.save()

@task
def update_mailing_list_settings(group_id):
    group = Group.objects.get(pk=group_id)
    if not group.mailing_list:
        return

    group.mailing_list.change_settings({
        'nondigest': {
            'msg_footer': group.get_message_footer(),
        },
        'digest': {
            'digestable': '0',      # disable digest mode.
        },
        'privacy/subscribing': {
            'advertised': '0',      # don't advertise list.
            'private_roster': '2',  # only list admin can see roster
            'generic_nonmember_action': '3', # Reject nonmember emails outright.
        },
        'archive': {
            'archive_private': '1', # private list archives
        },
    })
