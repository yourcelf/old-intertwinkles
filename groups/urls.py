from django.conf.urls.defaults import include, patterns, url

group_slug = '(?P<group_slug>[-_.0-9a-zA-Z]+)'

urlpatterns = patterns('groups.views',
    url(r'^$', 'manage_groups', name='groups_manage_groups'),
    url(r'^(?:(?P<group_id>\d+)/)?edit$', 'edit_group', name='groups_edit_group'),
    url(r'^accept_or_decline/(?P<membership_id>\d+)$', 'accept_or_decline_invitation', name='groups_accept_or_decline_invitation'),
    url(r'^show/{0}$'.format(group_slug), 'show_group', name='groups_show_group'),
    url(r'^search/{0}?$'.format(group_slug), 'search', name='groups_search'),
    url(r'^stuff/{0}$'.format(group_slug), 'browse_stuff', name='groups_browse_stuff'),
)
