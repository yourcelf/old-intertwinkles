membershipTableTemplate = "
  <tr>
    <th></th>
    <th>Email</th>
    <th>Role <i class='icon-question-sign role'></i></th>
    <th>Voting <i class='icon-question-sign voting'></i></th>
  </tr>
"
membershipRowTemplate = "
  <td>
    <a href='#' title='Remove' class='remove'><i class='icon-minus-sign'></i></a>
  </td>
  <td class='control-group'>
    <% if (person) { %>
      <%- person %>
    <% } else { %>
      <input type='text' value='<%= email %>' name='email'/>
    <% } %>
  </td>
  <td class='control-group'>
    <input type='text' value='<%= role %>' name='role' placeholder='Optional.' />
  </td>
  <td class='control-group'>
    <label>
      <input data-name='voting' type='checkbox' <% if (voting) { %> checked='checked' <% } %> />
      <input type='hidden' name='voting' value='<%= voting ? 1 : 0 %>' %>
      Can vote
    </label>
  </td>
"

class it.MembershipRow extends Backbone.View
  tagName: 'tr'
  template: _.template(membershipRowTemplate)
  events:
    'click .remove': 'triggerRemoval'
    'keydown input': 'triggerKeyDown'
    'keyup   input': 'checkEmpties'
    'change input[name=email]': 'validateEmail'
    'change input[type=checkbox]': 'checkboxFix'

  initialize: (options) ->
    @data = options.data

  render: =>
    @$el.addClass("form-row")
    if @data.errors
      @el.addClass("error")
    @$el.html(@template({
      email: @data.email
      role: @data.role
      voting: if @data.voting? then @data.voting else true
      person: @data.person
    }))
    if not @data.email and not @data.role
      @$el.addClass("empty")
    else
      @$el.removeClass("empty")

  checkboxFix: (event) =>
    check = $(event.currentTarget)
    @$("input[name=#{check.attr("data-name")}]").val(
      if check.is(":checked") then "1" else "0"
    )

  validateEmail: (event) =>
    email = $.trim($(event.currentTarget).val())
    valid = it.EMAIL_RE.test(email)
    td = @$("input[name=email]").parent()
    td.removeClass("error")
    @$("input[name=email]").parent().find(".error").remove()
    unless valid
      td.prepend(
        "<div class='error'><span class='help-inline'>Please enter a valid email address.</span></div>"
      )
      td.addClass("error")

  triggerRemoval: (event) =>
    @trigger "remove", this
    @remove()
    @trigger "checkEmpties", event
    return false

  triggerKeyDown: (event) =>
    if event.keyName() == '\r'
      next = @$el.next().find("input:first")
      if next.length > 0
        event.stopPropagation()
        next.select()
        return false

  checkEmpties: (event) =>
    isEmpty = true
    for input in [@$("input[name=email], input[name=role]")]
      if $(input).val() != ""
        isEmpty = false
        break
    @$el.toggleClass("empty", isEmpty)
    @trigger "checkEmpties", event

class it.MembershipTable extends Backbone.View
  tagName: 'table'
  template: _.template(membershipTableTemplate)
  initialize: (options) ->
    @memberships = options.memberships

  render: =>
    @$el.html(@template())
    @rows = []
    for m in @memberships
      @addRow(m)
    @rows[0].trigger("checkEmpties")
    $(".role").popover({
      title: "Role"
      content: "Roles are labels next to your name, such as 'president' or 'secretary'."
    })
    $(".voting").popover({
      title: "Voting"
      content: "By default, every member can vote.  If you uncheck <nobr>'Can vote'</nobr>, the member's votes will be counted separately."
    })

  addRow: (data) =>
    row = new it.MembershipRow(data: data)
    @$el.append(row.el)
    row.render()
    @bindRow(row)
    @rows.push(row)
    return row

  bindRow: (row) =>
    row.on "remove", => @rows = _.reject @rows, (r) -> r == row
    row.on "checkEmpties", (event) =>
      @$("tr.empty").not(row.$el).remove()
      if @$("tr.empty").length == 0
        @addRow({})

class it.Person extends Backbone.Model
class it.PersonCollection extends Backbone.Collection
  model: it.Person
