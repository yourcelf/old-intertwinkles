from urllib import urlencode

from django.db import models
from django.db.models import Q
from django.db.models.signals import post_syncdb, post_save
from django.utils.timezone import now as utcnow
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_noop as _
from django.conf import settings
from django.dispatch import receiver

from model_utils.managers import PassThroughManager
from django_mailman.models import List


class PQ(Q):
    accessor = ""
    def __init__(self, **kwargs):
        super(PQ, self).__init__(**dict(
            (self.prefix(k), v) for k,v in kwargs.items()
        ))

    def prefix(self, *args):
        return "__".join(a for a in (self.accessor,) + args if a)

class MembershipQuerySet(models.query.QuerySet):
    class MQ(PQ): accessor = ""

    def _membership_prefix(self, *args):
        return "__".join(a for a in (self.membership_accessor,) + args if a)

    def membership_active_q(self, when=None):
        return self.membership_current_q(when) & self.membership_accepted_q()

    def membership_current_q(self, when=None):
        when = when or utcnow()
        return self.MQ(start__lte=when) & (
                    self.MQ(end__isnull=True) | self.MQ(end__gte=when)
                )

    def membership_accepted_q(self):
        return self.MQ(invitation_accepted=True,
                       invitation_closed__isnull=False)

    def membership_refused_q(self):
        return self.MQ(invitation_accepted=False,
                       invitation_closed__isnull=False)

    def membership_invited_q(self):
        """
        Invitation is accepted, or invitation is open.
        """
        return self.MQ(invitation_closed__isnull=True) | self.MQ(invitation_closed__isnull=False, invitation_accepted=True)

    def active(self, when=None):
        return self.filter(self.membership_active_q(when=when))

    def on_mailing_list(self):
        return self.filter(subscribe_to_mailing_list=True)

    def lapsed(self, when=None):
        return self.filter(self.membership_accepted_q(), 
                           ~self.membership_current_q())

    def group_active(self):
        return self.filter(group__active=True)

    def group_inactive(self):
        return self.filter(group__active=False)

    def current_and_invited(self, when=None):
        return self.filter(
            self.membership_invited_q() & self.membership_current_q(when=when)
        )
    
    def overlapping_with(self, start, end=None):
        if end == None:
            return self.filter(Q(end__isnull=True) | Q(end__gte=start))
        else:
            return self.filter(
                    end__isnull=True, start__lte=end
                ) | self.filter(
                    end__isnull=False, start__lte=end, end__gte=start
                )

    def accepted(self):
        return self.filter(self.membership_accepted_q())

    def refused(self):
        return self.filter(self.membership_refused_q())

    def open_invitations(self):
        return self.current_and_invited().filter(invitation_closed__isnull=True)

    def current_for(self, user):
        return self.filter(user=user).current_and_invited()

class GroupQuerySet(MembershipQuerySet):
    class MQ(PQ):
        accessor = "membership"

    def membership_q(self, user):
        return self.MQ(user=user)

    def active_for(self, user, when=None, *args, **kwargs):
        qs = self.filter(*args, **kwargs).distinct()
        if not user.is_superuser:
            qs = qs.filter(self.membership_q(user), self.membership_active_q(when))
        return qs 

    def editable_for(self, user, when=None, *args, **kwargs):
        qs = self.filter(*args, **kwargs)
        if not user.is_superuser:
            qs = qs.filter(self.membership_q(user),
                           self.membership_active_q(when))
        return qs

    def can_vote(self, user):
        return self.current_for(user, membership__voting=True)

    def current_for(self, user, **kwargs):
        now = utcnow()
        return self.filter(
            (
                Q(membership__end__isnull=True) |
                Q(membership__end__gte=now)
            ),
            (
                Q(membership__invitation_accepted=True) |
                Q(membership__invitation_closed__isnull=True)
            ),
            membership__start__lte=now,
            membership__user=user,
            **kwargs
        )


class Membership(models.Model):
    user  = models.ForeignKey(User)
    group = models.ForeignKey('Group')
    start = models.DateTimeField(default=utcnow)
    end   = models.DateTimeField(blank=True, null=True)
    subscribe_to_mailing_list = models.BooleanField(default=True)
    # Invitation
    invitation_closed = models.DateTimeField(blank=True, null=True)
    invitation_accepted = models.BooleanField(default=False)
    invited_by = models.ForeignKey(User, related_name="invitations_sent",
           blank=True, null=True)
    # Roles
    role = models.CharField(max_length=100, default="", blank=True)
    voting = models.BooleanField(default=True)

    objects = PassThroughManager.for_queryset_class(MembershipQuerySet)()

    def is_current(self, when=None):
        when = when or utcnow()
        return self.start < when and (self.end is None or self.end > now)

    def has_joined(self):
        now = utcnow()
        return self.invitation_accepted and self.invitation_closed and \
                self.is_current(now)

    def accept_invitation(self, when=None):
        self.invitation_accepted = True
        self.invitation_closed = when or utcnow()
        self.save()

    def accept_or_decline_url(self):
        return reverse("groups_accept_or_decline_invitation", args=[self.pk])

    def end_membership(self, when=None):
        when = when or utcnow()
        self.end = when
        self.save()

    def clean(self):
        if self.start is None:
            raise ValidationError("Must specify a start date.")
        if self.end is not None and self.start > self.end:
            raise ValidationError("`Start` must come before `end`.")

    def __unicode__(self):
        return "%s, %s of %s" % (self.user.email, self.role, self.group)

    def refuse_invitation(self):
        self.invitation_accepted = False
        self.invitation_closed = utcnow()
        self.save()

    def __unicode__(self):
        return "%s, %s" % (self.group, self.user)

    class Meta:
        ordering = ['-voting', '-role']

class Group(models.Model):
    name = models.CharField(max_length=255, unique=True,
            verbose_name="Full name")
    slug = models.SlugField(unique=True, verbose_name='Short name',
            help_text="Letters, numbers, and dashes only (no spaces).  Choose carefully - this can't be changed later.")
    mailing_list = models.ForeignKey(List, blank=True, null=True)
    enable_mailing_list = models.BooleanField(default=True, help_text="Uncheck to disable the mailing list for this group.")
    active = models.BooleanField(default=True,
            help_text="Uncheck to archive this group (affects all members of the group).  Only uncheck if the group is no longer active.")
    logo = models.ImageField(upload_to="group_logos", height_field='logo_height', width_field='logo_width', null=True, blank=True)
    logo_height = models.IntegerField(null=True)
    logo_width = models.IntegerField(null=True)

    objects = PassThroughManager.for_queryset_class(GroupQuerySet)()

    def get_mailing_list_address(self):
        return "{0}@{1}".format(self.slug, settings.MAILMAN['email_domain'])

    def get_group_cc_mailto(self):
        emails = [m.user.email for m in self.membership_set.active()]
        if len(emails) == 0:
            return ""
        if len(emails) == 1:
            return emails[0]
        return "%s?%s" % (
            emails[0],
            "&".join([urlencode({'cc':e}) for e in emails[1:]])
        )


    def get_message_footer(self):
        return """
________________________________________________
This is an InterTwinkles group mailing list.
Find out more here: http://{0}{1}""".format(
                settings.SITE_URL, self.get_absolute_url()
            ).strip()

    def get_absolute_url(self):
        return reverse("groups_show_group", args=[self.slug])

    def get_edit_url(self):
        return reverse("groups_edit_group", args=[self.pk])

    def __unicode__(self):
        return self.name

if "notification" in settings.INSTALLED_APPS:
    from notification import models as notification

    @receiver(post_syncdb, sender=notification)
    def create_notice_types(app, created_models, verbosity, **kwargs):
        notification.create_notice_type("groups_invitation",
                _("Group Invitation"),
                _("New invitation to join a group"))

    @receiver(post_save, sender=Membership)
    def send_membership_notifications(sender, instance=None, **kwargs):
        # Only fire on creation
        if instance is None or not kwargs.get('created', False):
            return
        if instance.invitation_closed is not None:
            return
        if instance.user != instance.invited_by:
            notification.send(
                [instance.user],
                "groups_invitation",
                {'membership': instance},
                sender=instance.invited_by,
                on_site=False, # Not needed on-site -- will show up under groups.
            )

else:
    print "Skipping creation of NoticeTypes; notification app not found"
