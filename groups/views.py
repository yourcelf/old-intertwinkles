import json
import math
from urllib import urlencode
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import Http404
from django.template import loader, RequestContext
from django.utils.html import escape
from django.utils.timezone import now
from notification import models as notification

from profiles.utils import get_or_create_user, user_public_dict
from groups.models import Group, Membership
from timelines.models import Event
from embeds.models import Embed
from resolve.models import Proposal
from groups.forms import GroupForm, GroupSearchForm
from groups import tasks

@login_required
def search(request, group_slug=None):
    if group_slug:
        try:
            group = Group.objects.active_for(request.user).get(
                    slug=group_slug)
        except Group.DoesNotExist:
            raise Http404
        group_ids = [group.pk]
    else:
        group = None
        group_ids = Group.objects.active_for(request.user).values_list(
                'id', flat=True)

    q = request.GET.get('q', '')
    if q:
        form = GroupSearchForm(group_ids, request.GET)
        results = form.search()
        paginator = Paginator(results, 10)
        try:
            page = paginator.page(request.GET.get("page"))
        except PageNotAnInteger:
            page = paginator.page(1)
        count = results.count()
    else:
        count = 0
        page = None

    involvement = []
    objects = []
    if page:
        objects = page.object_list
        for result in objects:
            people_for_result = {}
            if result.object:
                recent_events = Event.objects.involving_model(result.object)
                for event in recent_events:
                    people_for_result[event.user_id] = event.user
                involvement.append(people_for_result)
            involvement.append({})

    return render(request, "groups/search.html", {
        'q': q,
        'querystr': ("&%s" % urlencode({'q': q})) if q else "",
        'group': group,
        'results': zip(objects, involvement),
        'page': page,
    })

@login_required
def browse_stuff(request, group_slug):
    try:
        group = Group.objects.active_for(request.user).get(slug=group_slug)
    except Group.DoesNotExist:
        raise Http404

    embeds = Embed.objects.active_for(request.user)
    proposals = Proposal.objects.active_for(request.user)
    if group.enable_mailing_list and group.mailing_list:
        recent_messages = group.mailing_list.listmessage_set.filter(thread_depth_denormalized=0).order_by('-date')[:10]
    else:
        recent_messages = None

    return render(request, "groups/stuff.html", {
        'group': group,
        'membership': group.membership_set.current_for(request.user).get(),
        'embeds': embeds,
        'proposals': proposals,
        'recent_messages': recent_messages,
    })


@login_required
def manage_groups(request):
    return render(request, "groups/manage_groups.html", {
        'active_memberships': list(request.user.membership_set.active().group_active()),
        'invitations': list(request.user.membership_set.open_invitations().group_active()),
        'lapsed_memberships': list(request.user.membership_set.lapsed()),
        'inactive_group_memberships': list(request.user.membership_set.active().group_inactive()),
    })

@login_required
@require_POST
def accept_or_decline_invitation(request, membership_id):
    invitation = get_object_or_404(Membership, pk=membership_id)
    if not invitation.user == request.user:
        raise PermissionDenied
    if request.POST.get('accept', None) is not None:
        invitation.accept_invitation()
        tasks.update_mailing_list_subscriptions.delay(
                group_id=invitation.group_id).get()
        messages.success(request,
            "Invitation accepted. Welcome to {0}!".format(invitation.group.name))
        return redirect(invitation.group.get_absolute_url())
    elif request.POST.get('decline', None) is not None:
        invitation.refuse_invitation()
        messages.success(request, "Invitation successfully declined.".format(invitation.group.name))
    return redirect("home")

@login_required
def show_group(request, group_slug):
    try:
        membership = Membership.objects.current_and_invited().get(
                user=request.user, group__slug=group_slug)
    except Membership.DoesNotExist:
        raise Http404
    group = membership.group
    if group.enable_mailing_list and group.mailing_list:
        recent_messages = group.mailing_list.listmessage_set.filter(thread_depth_denormalized=0).order_by('-date')[:10]
    else:
        recent_messages = None

    return render(request, "groups/show_group.html", {
        'membership': membership,
        'group': group,
        'recent_messages': recent_messages,
    })

@login_required
def edit_group(request, group_id=None):
    if group_id:
        try:
            group = Group.objects.editable_for(request.user).get(pk=group_id)
        except Group.DoesNotExist:
            raise Http404
        memberships = list(group.membership_set.current_and_invited())
        membership = group.membership_set.current_for(request.user)
    else:
        group = Group()
        memberships = [Membership(user=request.user, group=group,
                                  invitation_closed=now(),
                                  invitation_accepted=True)]
        membership = None

    t = loader.get_template("groups/partials/membership_display.html")
    membership_data = [{
            "email": escape(m.user.email),
            "role": escape(m.role),
            "voting": m.voting,
            "person": t.render(RequestContext(request, {'membership': m})),
        } for m in memberships]

    group_form = GroupForm(request.POST or None, request.FILES or None, instance=group)
    if group_form.is_valid():
        # Save group
        group = group_form.save()
        # Change membership
        form_data_by_email = {}
        for email, role, voting in zip(request.POST.getlist("email"),
                request.POST.getlist("role"),
                request.POST.getlist("voting")):
            if email:
                form_data_by_email[email] = {'role': role, 'voting': bool(int(voting))}
        desired_emails = set(form_data_by_email.keys())
        current_emails = set(
            [m.user.email for m in group.membership_set.current_and_invited()]
        )
        removed_emails = current_emails - desired_emails
        new_emails     = desired_emails - current_emails
        kept_emails    = desired_emails & current_emails

        if len(new_emails) + len(kept_emails) < 1:
            messages.error(request, "You must keep at least one group member.")
        else:
            for email in removed_emails:
                group.membership_set.current_and_invited().get(
                        user__email=email
                ).end_membership()

            for email in new_emails:
                m, created = Membership.objects.get_or_create(
                    group=group,
                    user=get_or_create_user(email=email),
                    invited_by=request.user,
                    voting=form_data_by_email[email]['voting'],
                    role=form_data_by_email[email]['role']
                )
                if email == request.user.email:
                    m.accept_invitation()

            for email in kept_emails:
                m = group.membership_set.current_and_invited().get(user__email=email)
                m.voting = form_data_by_email[email]['voting']
                m.role = form_data_by_email[email]['role']
                if email == request.user.email:
                    m.accept_invitation()
                else:
                    m.save()

            tasks.update_mailing_list_subscriptions.delay(
                    group_id=group.pk).get()

            if group.membership_set.active().filter(user=request.user).exists():
                return redirect("groups_show_group", group_slug=group.slug)
            else:
                messages.success(request, "Successfully left '{0}'.".format(group.name))
                return redirect("groups_manage_groups")
    else:
        # Salvage membership entries from POST to redisplay on form.
        existing = set(d['email'] for d in membership_data)
        for email, role, voting in zip(request.POST.getlist("email"),
                request.POST.getlist("role"),
                request.POST.getlist("voting")):
            if email not in existing:
                membership_data.append({
                    'email': email,
                    'role': role,
                    'voting': bool(int(voting)),
                })
    return render(request, "groups/edit_group.html", {
        'group': group,
        'membership': membership,
        'memberships': memberships,
        'form': group_form,
        'membership_data': json.dumps(membership_data),
    })
