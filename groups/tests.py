import datetime
from django.test import TestCase
from django.contrib.auth.models import User
from django.utils.timezone import now
from django.core import mail

from groups.models import Group, Membership
from profiles.models import Profile
from notification.models import get_notification_setting, NoticeType

class TestGroupMembership(TestCase):
    def setUp(self):
        self.u1 = User.objects.create_user(username="a", password="a")
        self.u2 = User.objects.create_user(username="b", password="b")
        self.u3 = User.objects.create_user(username="c", password="c")

    def test_group_creation(self):
        group = Group.objects.create(name="test", slug="test")
        group.membership_set.create(user=self.u1, voting=True)
        group.membership_set.create(user=self.u2, voting=False)
        self.assertEquals(set(Group.objects.can_vote(self.u1)), set([group]))
        self.assertEquals(set(Group.objects.can_vote(self.u2)), set())

    def test_query_sets(self):
        g1 = Group.objects.create(name="g1", slug="g1")
        u1g1mem = g1.membership_set.create(
                user=self.u1,
                voting=True,
                start=now() - datetime.timedelta(days=2),
                end=now() - datetime.timedelta(days=1),
        )
        u2g1mem = g1.membership_set.create(
                user=self.u2,
                voting=True,
                start=now() - datetime.timedelta(days=1),
                end=now() + datetime.timedelta(days=1),
        )
        self.assertEquals(set(Group.objects.can_vote(self.u1)), set())
        self.assertEquals(set(Group.objects.can_vote(self.u2)), set([g1]))
        self.assertEquals(set(g1.membership_set.current_and_invited()), set([u2g1mem]))
        # Active members have to be accepted, with invitation closed.
        self.assertEquals(set(g1.membership_set.active()), set())

        g2 = Group.objects.create(name="g2", slug="g2")

        # u1 is an accepted but now expired member of g1, and has no membership in g2.
        u1g1mem.invitation_accepted = True
        u1g1mem.invitation_closed = now()
        u1g1mem.save()

        # u2 is an accepted member of g2, but unaccepted member of g1.
        u2g2mem = g2.membership_set.create(
                user=self.u2,
                start=now() - datetime.timedelta(days=1),
                invitation_accepted=True,
                invitation_closed=now()
        )
        self.assertEquals(set(Group.objects.active_for(self.u1)), set())
        self.assertEquals(set(Group.objects.active_for(self.u2)), set([g2]))
        self.assertEquals(set(Group.objects.active_for(self.u3)), set())

    def test_active_active_for_equivalency(self):
        """
        We duplicate the logic in Membership.objects.active() and
        BaseGroupQuerySet.active_for(u), so ensure they are equivalent.
        """
        g1 = Group.objects.create(name="g1", slug="g1")
        g2 = Group.objects.create(name="g2", slug="g2")
        conditions = [
            # Name, Invitation accepted, invitation closed, membership lapsed, group slug
            ("u1",   False,  None,   False, "g1"),
            ("u2",   True,   None,   False, "g1"),
            ("u3",   False,  now(),  False, "g1"),
            ("u4",   True,   now(),  False, "g1"),
            ("u5",   False,  None,   True,  "g1"),
            ("u6",   True,   None,   True,  "g1"),
            ("u7",   False,  now(),  True,  "g1"),
            ("u8",   True,   now(),  True,  "g1"),
        ]
        for username, inv_acc, inv_closed, mem_lapsed, group_slug in conditions:
            group = Group.objects.get(slug=group_slug)
            user = User.objects.create(username=username)
            Membership.objects.create(user=user, group=g1,
                    invitation_accepted=inv_acc,
                    invitation_closed=inv_closed,
                    start=now() - datetime.timedelta(days=2),
                    end=now() - datetime.timedelta(days=1) if mem_lapsed else None,
            )
        active_for = set(["u4"])
        inactive_for = set(["u1", "u2", "u3", "u5", "u6", "u7", "u8"])
        self.assertEquals(
            set([m.user.username for m in g1.membership_set.active()]),
            active_for)
        for u in active_for:
            user = User.objects.get(username=u)
            self.assertEquals(set(Group.objects.active_for(user)), set([g1]))
        for u in inactive_for:
            user = User.objects.get(username=u)
            self.assertEquals(set(Group.objects.active_for(user)), set())

    def test_membership_overlaps(self):
        nowish = now()
        now_plus = lambda days: nowish + datetime.timedelta(days=days)
        group = Group.objects.create(name="test", slug="test")

        # Open-ended
        # Starting at 0 days, never ending.
        m = Membership.objects.create(
                group=group,
                user=self.u1,
                start=nowish,
                end=None)
        for start, end, overlaps in (
                    (-1, None, True), # starting before, never ends
                    (1, None, True),  # starting after, never ends.
                    (-2, -1, False),  # starting before, ending before.
                    (-2, 1, True),    # starting before, ending after.
                    (1, 2, True),     # starting after, ending after.
                ):
            self.assertEquals(m in Membership.objects.overlapping_with(
                now_plus(start), now_plus(end) if end is not None else None
            ), overlaps)
        m.delete()

        # Closed-ended
        # Starting at 0 days, ending at 3 days.
        m = Membership.objects.create(group=group, user=self.u1,
                start=nowish, end=now_plus(3))
        for start, end, overlaps in (
                    (-1, None, True), # starting before, never ends
                    (1, None, True),  # starting during, never ends
                    (4, None, False), # starting after, never ends
                    (-2, -1, False),  # starting before, ending before
                    (-2, 1, True),    # starting before, ending during
                    (-2, 4, True),    # starting before, ending after
                    (1, 2, True),     # starting during, ending during
                    (1, 4, True),     # starting during, ending after
                    (4, 6, False),    # starting after, ending after
                ):
            self.assertEquals(m in Membership.objects.overlapping_with(
                now_plus(start), now_plus(end) if end is not None else None
            ), overlaps)

class TestInvitations(TestCase):
    def setUp(self):
        self.u1 = User.objects.create_user(username="a", password="a")
        self.u2 = User.objects.create_user(username="b", password="b")


    def test_invite(self):
        group = Group.objects.create(name="test", slug="test")
        start = now() - datetime.timedelta(days=1)
        end   = now() + datetime.timedelta(days=1)

        inv = group.membership_set.create(
                user=self.u1,
                voting=True,
                role="president",
                start=start,
                end=end,
        )
        self.assertEquals(set(Membership.objects.open_invitations()), set([inv]))
        self.assertEquals(set(group.membership_set.active()), set())
        inv.accept_invitation()
        self.assertEquals(set(Membership.objects.open_invitations()), set())
        self.assertEquals(set(group.membership_set.active()), set([inv]))

        # Refuse a membership.
        inv2 = group.membership_set.create(
                invited_by=self.u1,
                user=self.u2,
                voting=True,
                role="president",
                start=start,
                end=end,
        )
        self.assertEquals(set(Membership.objects.open_invitations()), set([inv2]))
        self.assertEquals(set(self.u2.membership_set.open_invitations()), set([inv2]))
        inv2.refuse_invitation()
        self.assertEquals(set(Membership.objects.open_invitations()), set())
        self.assertEquals(set(Membership.objects.refused()), set([inv2]))
        self.assertEquals(set(group.membership_set.active()), set([inv]))

class TestNotifications(TestCase):
    def setUp(self):
        self.u1 = User.objects.create_user(username="one", email="one@example.com")
        self.u2 = User.objects.create_user(username="two", email="two@example.com")
        self.group = Group.objects.create(name="test", slug="test")
        p = Profile.objects.create(
                user=self.u1,
                mobile_number="5551234567",
                mobile_carrier="T-Mobile",
        )
        p = Profile.objects.create(
                user=self.u2,
                mobile_number="5552345678",
                mobile_carrier="T-Mobile",
        )
        # u1 wants notifications to sms and email; u2 doesn't.
        for user, medium, send in (
                    (self.u1, "email", True),
                    (self.u1, "sms", True),
                    (self.u2, "email", False),
                    (self.u2, "sms", False)):
            notice_setting = get_notification_setting(user,
                    NoticeType.objects.get(label="groups_invitation"),
                    medium)
            notice_setting.send = send
            notice_setting.save()

    def clear_outbox(self):
        for i in range(len(mail.outbox)):
            mail.outbox.pop()

    def test_group_invitations(self):
        m = Membership.objects.create(
                user=self.u1,
                invited_by=self.u2,
                group=self.group,
        )
        self.assertEquals(
            set(m.to[0] for m in mail.outbox),
            set(['one@example.com', '5551234567@tmomail.net'])
        )
        self.clear_outbox()

        # No notifications for second user; prefs forbid it.
        m = Membership.objects.create(
                user=self.u2,
                invited_by=self.u1,
                group=self.group,
        )
        self.assertEquals(len(mail.outbox), 0)
