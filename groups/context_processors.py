from django.conf import settings

from groups.models import Membership
from resolve.models import Proposal
from notification.models import Notice

def todos(request):
    if not request.user.is_authenticated():
        return {}
    
    groups_to_join = list(
            request.user.membership_set.select_related().open_invitations()
    )
    proposals_requiring_response = list(
        Proposal.objects.select_related().needing_attention_from(request.user)
    )
    notices = list(
        Notice.objects.notices_for(request.user, unseen=True, on_site=True)
    )
    unseen_notice_count = len(notices)
    has_old_notices = Notice.objects.notices_for(request.user, unseen=False).exists()
    return {
       'total_message_count': (
           unseen_notice_count + 
           len(groups_to_join) +
           len(proposals_requiring_response)
       ),
       'notice_unseen_count': unseen_notice_count,
       'has_old_notices': has_old_notices,
       'notices': notices,
       'groups_to_join': groups_to_join,
       'proposals_requiring_response': proposals_requiring_response,
       'USERVOICE_ENABLE': getattr(settings, "USERVOICE_ENABLE", False),
    }

