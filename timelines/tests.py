"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from datetime import timedelta
from django.test import TestCase
from django.utils.timezone import now

from timelines.models import Event
from django.contrib.auth.models import User
from groups.models import Group, Membership

class SimpleTest(TestCase):
    def setUp(self):
        self.u1 = User.objects.create(email="one@mockmyid.com", username="one")
        self.u2 = User.objects.create(email="two@mockmyid.com", username="two")
        self.g1 = Group.objects.create(name="one", slug="one")
        self.g2 = Group.objects.create(name="two", slug="two")
        Membership.objects.create(
                user=self.u1,
                group=self.g1,
                invitation_closed=now(),
                invitation_accepted=True
        )
        Membership.objects.create(
                user=self.u2,
                group=self.g2,
                invitation_closed=now(),
                invitation_accepted=True
        )
        # Lapsed membership -- should change nothing.
        Membership.objects.create(
                user=self.u2,
                group=self.g1,
                start=now() - timedelta(days=2),
                end=now() - timedelta(days=1),
                invitation_closed=now() - timedelta(days=2),
                invitation_accepted=True
        )
        # Not accepted invitation membership -- should change nothing.
        Membership.objects.create(
                user=self.u1,
                group=self.g2,
                start=now(),
                end=None,
                invitation_closed=now(),
                invitation_accepted=False
        )

    def test_current_for(self):
        e1 = Event.objects.create(
                key="one",
                user=self.u1,
                group=self.g1
        )
        e2 = Event.objects.create(
                key="two",
                user=self.u2,
                group=self.g2
        )
        self.assertEquals(set([e1]), set(Event.objects.current_for(self.u1)))
        self.assertEquals(set([e2]), set(Event.objects.current_for(self.u2)))
