from django.db import models
from django.contrib.auth.models import User
from django.utils.timezone import now

from model_utils.managers import PassThroughManager
from jsonfield import JSONField


from groups.models import Group
from groups.models import MembershipQuerySet, PQ

class EventQuerySet(MembershipQuerySet):
    class MQ(PQ):
        accessor = "group__membership"

    def get_key(self, model, event_str=""):
        return ".".join(
            (model._meta.app_label,
             model._meta.object_name,
             str(model.pk),
             event_str)
        )

    def involving_model(self, model, **kwargs):
        return self.filter(key__startswith=self.get_key(model), **kwargs)

    def touch(self, match=None, update=None):
        match = match or {}
        update = update or {}
        try:
            event = Event.objects.filter(**match)[0]
        except IndexError:
            event = Event()
        event.date = now()
        for key, val in update.iteritems():
            setattr(event, key, val)
        event.save()
        return event

class Event(models.Model):
    key   = models.CharField(max_length=255, db_index=True)
    date  = models.DateTimeField(default=now)
    user  = models.ForeignKey(User, null=True)
    group = models.ForeignKey(Group, null=True)
    data  = JSONField(blank=True)

    objects = PassThroughManager.for_queryset_class(EventQuerySet)()

    def to_dict(self):
        return {
                'key': self.key,
                'date': self.date,
                'user_id': self.user.pk,
                'group_id': self.group.pk,
                'data': self.data,
        }

    def __unicode__(self):
        return "%s: %s" % (self.key, self.pk)

    class Meta:
        get_latest_by = 'date'
